this.showTaskStatus();
this.showTaskDueDate();
function showTaskStatus() {
    $(function () {

        var table  = $('#dataTables-task').DataTable({
            async: true,
            ajax: {
                "url": "/api-show-all-task",
                "type": "GET",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            searching: false,
            columns: [
                {data: 'pName', "render":function (data,type,row) {
                        console.log(row)
                        return "<a href='project/"+row.p_code+"'>"+row.pName+"</a>"
                    },searchable: true},
                {data: 'assign_name'},
                {data: 'tSubject', "render":function (data,type,row) {
                        console.log(row)
                        return "<a href='project-task/"+row.id+"'>"+row.tSubject+"</a>"
                    },searchable: true},
                {data: 't_Done',"render":function (data,type,row) {
                    return " <div class=\"custom_done\" style='background-size:"+row.t_Done+"px 100px'>"+row.t_Done+"%"+"</div>";
                }},
                {data: 't_eDate',orderable: true,searchable: true},
                {data: 'updated_at',orderable: true,searchable: true},

            ]
        });

    });
}

function showTaskDueDate() {
    $(function () {

        var table  = $('#dataTables-due-date').DataTable({
            async: true,
            ajax: {
                "url": "/api-task-due-date",
                "type": "GET",
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            },
            searching: false,
            columns: [
                {data: 'pName', "render":function (data,type,row) {
                        console.log(row)
                        return "<a href='project/"+row.p_code+"'>"+row.pName+"</a>"
                    },searchable: true},
                {data: 'assign_name'},
                {data: 'tSubject', "render":function (data,type,row) {
                        console.log(row)
                        return "<a href='project-task/"+row.id+"'>"+row.tSubject+"</a>"
                    },searchable: true},
                {data: 't_Done',orderable: true,searchable: true},
                {data: 't_eDate',orderable: true,searchable: true},
                {data: 'updated_at',orderable: true,searchable: true},

            ]
        });

    });
}