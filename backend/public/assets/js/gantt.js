this.callApi()
function callApi() {
    $.ajax({
        url: "/api-grant-chart",
        type: "get",
        beforeSend: function () {
            $('#wait').show();
        },
        complete: function () {
            $('#wait').hide();
        },
        dateType: "json",
        success: function (result) {
            var resultJson = result;
            console.log(result)
            showGrantChart(resultJson);
        }
    });
}

function showGrantChart(resultJson) {
    var date_now = new Date();
    console.log(date_now)
    $(function () {
        $("#GanttContainer").ejGantt({
            scheduleHeaderSettings: {
                weekHeaderFormat: "MMM yyyy",
                dayHeaderFormat: "d",
                hourHeaderFormat: "HH",
                monthHeaderFormat: "MMM",
                yearHeaderFormat : "yyyy",
                weekendBackground: '#F2F2F2'
            },
            readOnly: true,
            taskbarClick: function(args) {
                var taskbarElement = args.taskbarElement,
                    taskData = args.data;
                    target = args.target;
                var idTask = taskData.item.TaskID;
                console.log(taskData.item.TaskID)
                showModal(idTask)
            },
            parentTaskbarBackground: "#0f6faa",
            dataSource: resultJson,
            taskIdMapping: "TaskID",
            taskNameMapping: "TaskName",
            scheduleStartDate: "05/05/2019",
            scheduleEndDate: "12/14/2019",
            startDateMapping: "StartDate",
            endDateMapping: "EndDate",
            durationMapping: "duration",
            progressMapping: "Progress",
            childMapping: "SubTasks",
            treeColumnIndex: 1,
            resourceNameMapping: "resourceName",
        });
    });
    $("#openAddDialog").click(function(){
       alert("Ok")
    });
    $("#openEditDialog").click(function(){
        alert("Ok")
    });
}

function showModal(idTask) {
    var id = idTask;
    $.ajax({
        url: "/apiShowDetailTask",
        type: "get",
        data: {
            'id': id
        },
        beforeSend: function () {
            $('#wait2').show();
        },
        complete: function () {
            $('#wait2').hide();
        },
        dateType: "json",
        success: function (result) {

            console.log(result.task[0])

            var process = result.task[0].t_Process;
            var task_type = result.task[0].t_Type;
            var htmlResult = "";
            var html_process = "";
            var option_process = "";
            var html_Done = "";
            var html_type = "";
            var html_sub_task = "";
            var html_logs = "";
            var html_process = "";
            var html_Done_List = "";

            switch(parseInt(process,10)) {
                case(1):
                    html_process = "<button disabled class='btn btn-sm btn-primary'>New</button>"
                    break;
                case(2):
                    html_process = "<button disabled class='btn btn-sm btn-info'>In Progress</button>"
                    break;
                case(3):
                    html_process = "<button disabled class='btn btn-sm btn-warning'>Resolved</button>"
                    break;
                case(4):
                    html_process = "<button disabled class='btn btn-sm btn-success'> Done</button>"
                    break;
                case(5):
                    html_process = "<button disabled class='btn btn-sm btn-dark'>Closed</button>"
                    break;
                default:
                    html_process = "<button disabled class='btn btn-sm btn-primary'>gs</button>"
            }

            switch(task_type) {
                case(1):
                    html_type = "<button disabled type='button' class='btn btn-sm btn-danger'> Bug </button>"
                    break;
                case(2):
                    html_type = "<button disabled type='button' class='btn btn-sm btn-primary'> Development </button>"
                    break;
                case(3):
                    html_type = "<button disabled type='button' class='btn btn-sm btn-info'> Unit Testing </button>"
                    break;
                case(4):
                    html_type = "<button disabled type='button' class='btn btn-sm btn-warning'> Update </button>"
                    break;
                case(5):
                    html_type = "<button disabled type='button' class='btn btn-sm btn-link'> Other </button>"
                    break;
            }
            var jsonUser = result.user;
            var jsonSubTask = result.sub_tasl;
            var jsonLogTask = result.log;
            var htmlUser = "";
            Object.keys(jsonUser).forEach(function(key) {
                var user_id = jsonUser[key].user_id;
                var username = jsonUser[key].u_nickname;
                if(user_id != result.task[0].assign_id)
                {
                    htmlResult += "<option value='"+user_id+"'>"+username+"</option>";
                }
                htmlUser = "<option selected  value='"+result.task[0].assign_id+"'>"+result.task[0].u_nickname+"</option>";
            })
            console.log(jsonUser)

            var id = result.task[0].t_Done;
            html_Done += "<option selected value='"+id+"'>"+id+" %</option>";
            html_Done_List = "  <option value=\"10\">10 %</option>\n" +
                " <option value=\"20\">20 %</option>\n" +
                " <option value=\"30\">30 %</option>\n" +
                " <option value=\"40\">40 %</option>\n" +
                "  <option value=\"50\">50 %</option>\n" +
                "<option value=\"60\">60 %</option>\n" +
                "  <option value=\"70\">70 %</option>\n" +
                "  <option value=\"80\">80 %</option>\n" +
                " <option value=\"90\">90 %</option>\n" +
                "<option value=\"100\">100 %</option>";

            var name = result.task[0].t_Process;


            option_process += "<option selected value='"+name+"'>"+name+" </option>"
            switch(parseInt(process,10)) {
                case(1):
                    option_process = "<option selected value='"+name+"'>New</option><option value='2'>In Process</option><option  value='3'>Resolved</option><option  value='4'>Done</option><option  value='5'>Closed</option><option  value='6'>Review</option>"
                    break;
                case(2):
                    option_process = "<option selected value='"+name+"'>In Process </option><option value='1'>New</option><option  value='3'>Resolved</option><option  value='4'>Done</option><option  value='5'>Closed</option><option  value='6'>Review</option>"
                    break;
                case(3):
                    option_process = "<option selected value='"+name+"'>Resolved </option><option value='2'>In Process</option><option  value='1'>New</option><option  value='4'>Done</option><option  value='5'>Closed</option><option  value='6'>Review</option>"
                    break;
                case(4):
                    option_process = "<option selected value='"+name+"'>Done </option><option value='2'>In Process</option><option  value='3'>Resolved</option><option  value='1'>New</option><option  value='5'>Closed</option><option  value='6'>Review</option>"
                    break;
                case(5):
                    option_process = "<option selected value='"+name+"'>Closed </option><option value='2'>In Process</option><option  value='3'>Resolved</option><option  value='4'>Done</option><option  value='1'>New</option><option  value='6'>Review</option>"
                    break;
                case(6):
                    option_process = "<option selected value='"+name+"'>Review </option><option value='2'>In Process</option><option  value='3'>Resolved</option><option  value='4'>Done</option><option  value='1'>New</option><option  value='1'>New</option>"
                    break;
                default:
                    option_process = "<option selected value='"+name+"'>New</option><option value='2'>In Process</option><option  value='3'>Resolved</option><option  value='4'>Done</option><option  value='5'>Closed</option><option  value='6'>Review</option>"
            }

            Object.keys(jsonSubTask).forEach(function(key) {
                if (jsonSubTask[key].t_Done ===  0)
                {
                    html_sub_task += "<tr><td>"+jsonSubTask[key].id +"</td><td>"+jsonSubTask[key].tSubject +"</td><td>"+jsonSubTask[key].t_sDate+"</td><td>"+jsonSubTask[key].t_eDate+"</td><td>"+jsonSubTask[key].u_nickname +"</td><td>"+jsonSubTask[key].t_Done+"<td><a class='btn btn-sm btn-success'  href='/project-task/"+ jsonSubTask[key].id +"/edit'> <i class='fas fa-edit'></i></a>  <a class='btn btn-sm btn-danger'  href='/delete-sub-task/"+ jsonSubTask[key].id +"'> <i class='fas fa-trash-alt'></i></a></td></tr>";

                }
                else {
                    html_sub_task += "<tr><td>"+jsonSubTask[key].id +"</td><td>"+jsonSubTask[key].tSubject +"</td><td>"+jsonSubTask[key].t_sDate+"</td><td>"+jsonSubTask[key].t_eDate+"</td><td>"+jsonSubTask[key].u_nickname +"</td><td>"+jsonSubTask[key].t_Done+"<td><a class='btn btn-sm btn-success'  href='/project-task/"+ jsonSubTask[key].id +"/edit'> <i class='fas fa-edit'></i></a></td></tr>";
                }
            })
            //Logs
            Object.keys(jsonLogTask).forEach(function(key) {
                html_logs += "<li class='list-group-item'>"+jsonLogTask[key].l_status+" " + jsonLogTask[key].l_content +"</li>"
            })

            $("#create_sub_task").html("<a class='btn btn-success' href='/add-member-task/"+result.task[0].id+"'>Create SubTask</a>");
            $("#delete").html("<a href='/delete-sub-task/"+result.task[0].id+"' class='btn btn-danger'>Delete Task</a>")
            $("#id").val(result.task[0].id)
            $("#title").html(result.task[0].tSubject);
            $("#process").html(html_process);
            $("#t_task_type").html(html_type);
            $("#project_name").html(result.task[0].pName);
            $("#assign_name").html(result.task[0].u_nickname);
            $("#start_date").val(result.task[0].t_sDate);
            $("#end_date").val(result.task[0].t_eDate);

            $("#assign_user").html(htmlUser).append(htmlResult);

            $("#t_Done").html(html_Done).append(html_Done_List);
            $("#t_Process").html(option_process);


            $("#tabel_done").html(result.task[0].t_Done)
            $("#modal-title").html(result.task[0].tSubject)
            $("#url").html("<a href='/project-task/"+result.task[0].id+"' id=\"view\" class=\"btn btn-sm btn-info btn-round\" href=\"\"> <i class=\"fas fa-eye\"></i></a>")
            $("#sub_task").append(html_sub_task)
            $("#logs").html("").append(html_logs)
            var parent_id = result.task[0].parent_id;

        }
    });

    $('#Task_detail').modal('show').on("hidden.bs.modal", function () {
        var htmlNull = "";
        $("#assign_user").html(htmlNull);
        $("#tabel_done").html(htmlNull)
        $("#sub_task").html(htmlNull)
        $("#logs").html(htmlNull)
    });
}