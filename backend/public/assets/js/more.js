this.callApi();
function callApi() {
    $.ajax({
        url: "/api-pie-chart",
        type: "get",
        beforeSend: function () {
            $('#wait').show();
        },
        complete: function () {
            $('#wait').hide();
        },
        dateType: "json",
        success: function (result) {
            var resultJson = result.chart1;
            var resultQuarterly = result.chart2;

            console.log(result)
            showPieChart(resultJson);
            // quarterly(resultQuarterly);
        }
    });
}

function showPieChart(resultJson) {
    //foreach
    var htmlResult = "";
    Object.keys(resultJson).forEach(function (key) {
        if (resultJson[key].cnt_total_task > 0) {
            var total_done = Math.round(resultJson[key].cnt_task_done / resultJson[key].cnt_total_task * 100);
            var total = 100 - total_done;

            htmlResult = " <div class=\"col-md-3\"><a target='_blank' href='/project/" + resultJson[key].project_id + "'><h2 class=\"project_name text-center\">" + resultJson[key].pName + " - " + total_done + "%" + "</h2></a><div id=\"project_" + resultJson[key].project_id + "\" class=\"ct-chart ct-perfect-fourth\"></div>";
            $("#pie").append(htmlResult)


            console.log(total)

            //set pie
            new Chartist.Pie("#project_" + resultJson[key].project_id, {
                series: [total_done, total]
            }, {
                donut: true,
                donutWidth: 60,
                donutSolid: true,
                startAngle: 270,
                total: 200,
                showLabel: true
            });
        }
    })

}


