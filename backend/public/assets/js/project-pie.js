this.callApi();
function callApi() {
    var id = document.getElementById("project_id").value;
    console.log(id)
    $.ajax({

        url: "/api-show-count-project-task",
        type: "get",
        data: {
            'id': id
        },
        beforeSend: function () {
            $('#wait').show();
        },
        complete: function () {
            $('#wait').hide();
        },
        dateType: "json",
        success: function (result) {
            var resultJson = result;
            console.log(result)
            showPieChart(resultJson);
        }
    });
}

function showPieChart(resultJson) {
    if (resultJson.total[0].total > 0) {
        var total_done = Math.round(resultJson.done[0].total / resultJson.total[0].total * 100);
        var total = 100 - total_done;
        var htmlResult = "<h2 class='project_name text-center'>Process " + total_done + "%" + "</h2>";
        var remainder = resultJson.total[0].total - resultJson.done[0].total - resultJson.delay[0].total;
        $("#pie").html(htmlResult)
        $('#total').html(resultJson.total[0].total);
        $('#done').html(resultJson.done[0].total);
        $('#delay').html(resultJson.delay[0].total);
        $('#remainder').html(remainder)


        //set pie
        new Chartist.Pie("#project", {
            series: [total_done, total]
        }, {
            donut: true,
            donutWidth: 60,
            donutSolid: true,
            startAngle: 270,
            total: 200,
            showLabel: true
        });
    }

}