$(function () {
    $('#dataTables-task').DataTable({
        async: true,
        ajax: {
            "url": "/show-data-user",
            "type": "GET",
            "headers": {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        columns: [
            {
                data:   null,
                render: function ( data, type, row ) {
                    var id = row.id;
                    return "<input name='checked_id[]' id='arrUser' type='checkbox' class='checkbox editor-active' value="+id+">";
                },
                className: "dt-body-center"
            },
            {data: 'id', orderable: true},
            {data: 'name', searchable: true},
            {data: 'u_nickname', searchable: true},
            {data: 'email', orderable: true, searchable: true},
            {data: 'c_name', orderable: true, searchable: true},
            {
                data: 'u_auth', "render": function (data, type, row) {
                    var status = row.u_auth;
                    var id = row.id;
                    if (status === null) {
                        return "<a data-toggle=\"tooltip\" title=\"Membership approval \" href='admin-user-confirm/"+id+"'  class='btn btn-sm btn-register btn-info'><i class=\"fas fa-user-check\"></i></a>";
                    } else {
                        return '<span class="text-success">Active</span>';
                    }
                }
            },
            {data: 'is_Admin', "render":function (data,type,row) {
                    var status = row.is_Admin;
                    var id = row.id;
                    console.log(status)
                    if(status != null){
                        return '<span class="text-danger">Manager</span>';
                    }else
                    {
                        return '<span class="text-success">User</span>';
                    }
                }},
            {data: 'action', orderable: false, searchable: true,class:"text-center"},
        ]
    });
});