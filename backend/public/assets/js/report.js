
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
var yyyy = today.getFullYear();

today = yyyy + '-' + mm + '-' + dd;

console.log(today)

$("#start_date").val(today);

this.callApi();

function callApi() {
    var id = document.getElementById("id").value;
    console.log(id)
    $.ajax({
        url: "/report-apiShowCountTask",
        type: "get",
        data: {
            'id': id
        },
        beforeSend: function () {
            $('#wait').show();
        },
        complete: function () {
            $('#wait').hide();
        },
        dateType: "json",
        success: function (result) {
            var resultJson = result;
            showPieChart(resultJson);

            var resultTask = result.task.data;
            if (resultJson.total[0].total == 0) {
                $("#task").html("")
            } else {
                Object.keys(resultTask).forEach(function (key) {

                    $("#task").append("<tr><td></td><td>" + resultTask[key].u_nickname + "</td><td>" + resultTask[key].tSubject + "</td><td></td><td>" + resultTask[key].t_Done + "</td><td>" + resultTask[key].t_eDate + "</td></tr>")
                })
            }


        }
    });
}
function showPieChart(resultJson) {
console.log(resultJson)
    if (resultJson.total[0].total > 0) {
        var total_done = Math.round(resultJson.done[0].total / resultJson.total[0].total * 100);
        var total = 100 - total_done;
        var htmlResult = "<h2 class='project_name text-center'>"+resultJson.pName + " "+ total_done + "%" + "</h2>";
        var remainder = resultJson.total[0].total - resultJson.done[0].total - resultJson.delay[0].total;
        $("#pie").html(htmlResult)
        $('#total').html(resultJson.total[0].total);
        $('#done').html(resultJson.done[0].total);
        $('#delay').html(resultJson.delay[0].total);
        $('#remainder').html(remainder)


        //set pie
        new Chartist.Pie("#project", {
            series: [total_done, total]
        }, {
            donut: true,
            donutWidth: 60,
            donutSolid: true,
            startAngle: 270,
            total: 200,
            showLabel: true
        });

}}