$(function () {
    var table = $('#dataTables-task').DataTable({
        async: true,
        ajax: {
            "url": "/api-show-task-today",
            "type": "GET",
            "headers": {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        },
        searching: false,
        columns: [
            {data: 'pName', "render":function (data,type,row) {
                console.log(row)
                    return "<a href='project/"+row.p_code+"'>"+row.pName+"</a>"
                }},
            {data: 'assign_name'},
            {data: 'tSubject', "render":function (data,type,row) {
                    console.log(row)
                    return "<a href='project-task/"+row.id+"'>"+row.tSubject+"</a>"
                }},
            {data: 't_Done', orderable: true, searchable: true},
            {data: 't_eDate', orderable: true, searchable: true},
            {data: 'updated_at', orderable: true, searchable: true},
        ]
    });

});

