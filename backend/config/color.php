<?php
use Carbon\Carbon;
return [
    'PLAN' => '#E29D59', // ko co
    'DEV' => '#5cade5', // development
    'TEST' => '#FA7268', //unit test
    'UPDATE' => '#9cc161', //update
    'ETC' => '#f4a556', //other
    'DEBUG' => '#72d3bc' //bug
];
