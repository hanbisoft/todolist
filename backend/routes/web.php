<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Route::group(/**
 *
 */ ['middleware' => ['CheckVeri']], function () {
    Route::get('/', function() {
        return redirect('admin-dashboard');
    });
    Route::get('/admin-dashboard','DashboardController@index')->name('admin-dashboard');
    Route::get('/api-show-task-today','DashboardController@showMyTaskToday');

    Route::resource('/user-profile','UserController');
    Route::resource('/project','ProjectController');

    //project
    Route::get('/add-member/{id}','ProjectController@addMember')->name('project.addMember');
    Route::post('/createMember','ProjectController@createMember')->name('project.createMember');
    Route::get('/project-delete','ProjectController@destroyProject')->name('project-delete');
    Route::get('/api-show-count-project-task','ProjectController@apiCountTaskProject')->name('api-show-count-project-task');
    //download
    Route::post('/download','DownloadController@download')->name('download');

    Route::resource('/project-item','ProjectItemController');

    //task
    Route::resource('/project-task','TaskController');
    Route::get('/my-task','TaskController@myTask')->name('my-task');
    Route::get('/add-member-task/{id}','TaskController@addMember')->name('add-member-task');
    Route::post('/createMemberSubtask','TaskController@createMemberSubTask')->name('createMemberSubTask');
    Route::post('/update-task','AdminTaskController@updateTask')->name('update-task');


    Route::get('/show-task','TaskController@apiShowTask')->name('api.ShowTask');

    Route::post('update-status-task','ProjectController@UpdateProcess')->name('project.update-process-task');

    Route::get('/show-my-task-data','TaskController@showMyTaskData')->name('api.show-my-task-data');

    Route::get('/show-data-project','ProjectController@showData')->name('api.project.showData');

    Route::get('/apiShowDetailTask',"TaskController@apiShowDetailTask")->name('apiShowDetailTask');
    //calendar

    Route::get('/show-calendar-my-task','TaskController@calendarMyTask')->name('show-calendar-my-task');
    Route::get('/api-show-my-task','TaskController@apiShowMyTask')->name('api.ShowMyTask');
    Route::get('/api-search-my-task','TaskController@apiSearchMyTask')->name('api.SearchMyTask');
    Route::get('/search-my-task','TaskController@searchMyTaskCalendar')->name('searchMyTaskCalendar');

    //only admin
    Route::resource('/admin-task','AdminTaskController');
    Route::get('/api-show-all-task','AdminTaskController@showAllTask')->name('api-show-all-task');
    Route::get('/calendar-admin-task','AdminTaskController@calendarTaskAdmin')->name('calendar-admin-task');
    Route::get('/api-show-admin-task','AdminTaskController@apiShowAdminCalendar')->name('api.ShowAdminTask');
    Route::get('/search-calendar-admin','AdminTaskController@searchCalendarAdmin')->name('search.CalendarAdmin');
    Route::get('/show-all-user','UserController@showUser')->name('show-all-user');
    Route::get('/api-task-due-date','AdminTaskController@showTaskDueDate')->name('api-task-due-date');


    //Report
    Route::get('/report','ReportController@index')->name('report.index');
    Route::get('/report-apiShowCountTask','ReportController@apiShowCountTask')->name('report.apiShowCountTask');

    //update task by api
    Route::get('/api-update-task','AdminTaskController@apiUpdateTask')->name('apiUpdateTask');

    //company
    Route::resource('/company','AdminCompanyController');
    Route::get('/show-data-company','AdminCompanyController@showData')->name('show-data-company');
    Route::get('/company-delete','AdminCompanyController@destroyCompany')->name('destroy-company');

    //user
    Route::resource('/admin-user','AdminUserController');
    Route::get('/show-data-user','AdminUserController@showData')->name('show-user-company');
    Route::get('/admin-user-confirm/{id}','AdminUserController@updateUser')->name('updateUser');
    Route::get('/user-delete','AdminUserController@userDelete')->name('user-delete');
    Route::post('/user-delete-all','AdminUserController@userDeleteAll')->name('user-delete-all');
    Route::get('/upgrade-manager/{id}','AdminUserController@upgradeManger')->name('upgrade-manager');
    Route::get('/upgrade-user/{id}','AdminUserController@updateMember')->name('upgrade-user');

    //grant chart
    Route::resource('/grant-chart',"GrantChartController");
    //api grant chart
    Route::get('/api-grant-chart','GrantChartController@apiGrantChart')->name('api-grant-chart');
    Route::get('/search-gantt-chart','GrantChartController@searchGantt')->name('search-gantt-chart');
    Route::get('/api-search-gantt-chart','GrantChartController@apiSearchGanttChart')->name('api-search-gantt-chart');

    //delete sub task
    Route::get('/delete-sub-task/{id}','AdminTaskController@deleteSubTask')->name('delete-sub-task');

    //gantt
    Route::get('/api-pie-chart','AdminChartController@apiPieChart')->name('api-pie-chart');

    //project more
    Route::get('/project-more', function (){return view('admin.project.more');})->name('project.more');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/error-not-verify', function (){
   return view('errors.500');
});

Route::get('change-language/{locale}', 'HomeController@changeLanguage')->name('user.change-language');

Route::post('/uploads-file',function (){
    return "OKKOKOKOK";
});