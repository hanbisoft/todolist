<?php
return array(
    "dashboard" => "Bảng điều khiển",
    "project" => "Project",
    "task_assign_me" => " Tasks Assigned To Me",
    "gantt_chart" => "Gantt Chart",
    "calendar_all_task" => "Calendar All Task",
    "company_list" => "Company List",
    "report" => "Report",
    "user_list" => "User List",
    "new_task" => "New Task",
    "more" => "More",
    "delay_work" => "Delay work",
    "project_name" => "Project name",
    "date" => "Date",
    "to" => "To",
    "project_type" => "Project type",
    "amount" => "Amount",
    "action" => "Action",
    "developer_screen" => "Developer screen",

    ///dashboard
    "project_percent" => "1. Percent of project completion",
    "workload_by_the_employee" => "2. Workload by the employee (Quarterly)",
    "task_status_today" => "3. Task Status (Today)",
    "delay_work" => "Delay work",
    "assign_name" => "Assign name",
    "task_name" => "Task name",
    "done" => "Done %",
    "due_date" => "Due date",
    "last_modified_date" => "Last modified date",
    "project_type" => "Project type",
    "process" => "Process",
    "team_member" => "Team Member",
    "attachment_project" => "Attachment Project",
    "add_member" => "Add Member",
    "date" => "Date",
    "to" => "To"

    //
);