

@extends('layouts.app')

@section('content')
    <div class="container jumbotron text-xs-center">
        <h1 class="display-3">Thank You!</h1>
        <p class="lead">Your account is awaiting confirmation from the manager.</p>
        <hr>
    </div>
@endsection
