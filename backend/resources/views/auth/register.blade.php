<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.12
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Register</title>
    <!-- Icons-->
    <link href="{{asset('vendor/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/flag-icon.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{asset('vendor/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('coreui/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/pace-progress/css/pace.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"
          rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>
    <style>
        body {
            background-image: url('images/bg-login.jpg');
        }

        .bg {
            background-image: url('images/bg-sigup.jpg') !important;
            background-size: 100%;
            background: #777;
            opacity: 1.5;
        }

        .card-body {
            padding-top: 4em;
        }

        .btn_register {
            color: #fff;
            background-color: #6AC3CC !important;
            border-color: #6AC3CC !important;
            border-radius: 25px;
        }

        .btn_login {
            background-color: #6AC3CC !important;
            border-color: #6AC3CC !important;
            border-radius: 25px;
        }

        .fw_pass {
            color: #6AC3CC;
        }

        .group_input {
            width: 100%;
            display: inherit;
            border: 1px solid #e4e7ea;
            border-radius: 25px;
        }

        .input-group-text {
            background: none !important;
            border: none !important;
        }

        #c_code, #email, #password, #name, #password-confirm,#u_nickname {
            width: 80% !important;
            border: none;
        }
    </style>
</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mx-4">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="card-body p-4">
                        <h1>Register</h1>
                        <p class="text-muted">Create your account</p>
                        <div class="input-group mb-3">
                            <div class="group_input">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="far fa-building"></i>
                                </span>
                                </div>
                                <select name="c_code" id="c_code"
                                        class="form-control{{ $errors->has('c_code') ? ' is-invalid' : '' }}" required>
                                    <option value="">Please select company</option>
                                    @foreach($company as $value)
                                        <option value="{{$value->id}}">{{ $value->c_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('c_code'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('c_code') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="group_input">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-user"></i></span>
                                </div>
                                <input id="name" type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                       value="{{ old('name') }}" placeholder="Name" required autofocus>
                            </div>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback"
                                      role="alert"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="group_input">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-user"></i></span>
                                </div>
                                <input id="u_nickname" type="text"
                                       class="form-control{{ $errors->has('u_nickname') ? ' is-invalid' : '' }}"
                                       name="u_nickname"
                                       value="{{ old('u_nickname') }}" placeholder="Nick Name" required autofocus>
                            </div>
                            @if ($errors->has('u_nickname'))
                                <span class="invalid-feedback"
                                      role="alert"><strong>{{ $errors->first('u_nickname') }}</strong></span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="group_input">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input id="email" type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}" placeholder="Email" required>
                            </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <div class="group_input">
                            <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="icon-lock"></i>
                          </span>
                            </div>
                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password" placeholder="Password" required>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="input-group mb-4">
                            <div class="group_input">
                            <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="icon-lock"></i>
                          </span>
                            </div>
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" placeholder="Password confirmation" required>
                            </div>
                        </div>
                        <button class="btn_register btn btn-block btn-success" type="submit">Create Account</button>
                    </div>
                </form>
                <div class="card-footer p-4">
                    <div class="row">
                        <div class="col-6">
                            <a class="btn_login btn btn-block btn-facebook" href="/login">
                                <span>Login</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
