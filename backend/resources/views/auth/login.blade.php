<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.12
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Login</title>
    <!-- Icons-->
    <link href="{{asset('vendor/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/flag-icon.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{asset('vendor/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('coreui/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/pace-progress/css/pace.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"
          rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>
    <style>
        body {
            background-image: url('images/bg-login.jpg');
        }

        .bg {
            background-image: url('images/bg-sigup.jpg') !important;
            background-size: 100%;
            background: #777;
            opacity: 1.5;
        }

        .card-body {
            padding-top: 4em;
        }

        .btn_register {
            color: #fff;
            background-color: #6AC3CC !important;
            border-color: #6AC3CC !important;
            border-radius: 25px;
        }

        .btn_login {
            background-color: #6AC3CC !important;
            border-color: #6AC3CC !important;
            border-radius: 25px;
        }

        .fw_pass {
            color: #6AC3CC;
        }

        .group_input {
            width: 100%;
            display: inherit;
            border: 1px solid #e4e7ea;
            border-radius: 25px;
        }

        .input-group-text {
            background: none !important;
            border: none !important;
        }

        #c_code, #email, #password {
            width: 80% !important;
            border: none;
        }
    </style>
</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                    </button>
                    <span>{{ session()->get('error') }}</span>
                </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card-group">
                <div class="card p-4">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="card-body">

                            @csrf
                            <h1>Login</h1>
                            <p class="text-muted">Sign In to your account</p>
                            <div class="input-group mb-3">
                                <div class="group_input">
                                    <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="far fa-building"></i>
                                </span>
                                    </div>
                                    <select name="c_code" id="c_code"
                                            class="form-control{{ $errors->has('c_code') ? ' is-invalid' : '' }}"
                                            required>
                                        <option value="">Please select company</option>
                                        @foreach($company as $value)
                                            <option value="{{$value->id}}">{{ $value->c_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('c_code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('c_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="input-group mb-3">
                                <div class="group_input">
                                    <div class="input-group-prepend">
                                <span class="input-group-text">
                                  <i class="icon-user"></i>
                                </span>
                                    </div>
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="input-group mb-4">
                                <div class="group_input">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="icon-lock"></i></span>
                                    </div>
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn_login btn btn-primary px-4" type="submit">Login</button>
                                </div>
                                <div class="col-6 text-right">
                                    <a class="fw_pass" href="{{ route('password.request') }}" class="">Forgot
                                        password?</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="bg card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <h2>Sign up</h2>
                            <p>If you do not have an account <br> please register here</p>
                            <a href="/register" class="btn_register btn btn-primary active mt-3">Register Now!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
