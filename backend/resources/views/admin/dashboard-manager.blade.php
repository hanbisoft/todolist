@extends('admin.master')
@section('title')
    Dashboard
@endsection
@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/chartist.min.css')}}">
    <script src="{{asset('scripts/chartist.min.js')}}"></script>
    <div class="row bg-top" id="pie">
        <div class="col-md-12" style="margin-bottom: 3em">
            <h2>  {{ trans('message.project_percent') }} &ensp; <a  href="{{route('project.more')}}" class="btn btn_register btn-info">Read More</a></h2>
        </div>
            <div id="wait"></div>
        </div>
    <div class="row bg-top">
        <div class="col-md-12" style="margin-bottom: 2em">
            <h2> {{ trans('message.workload_by_the_employee') }}</h2>
        </div>
         <div class="col-md-12" id="quarterly"></div>
    </div>
    <div class="row bg-top">
        <div class="col-md-12">
            <h2>{{ trans('message.task_status_today') }}</h2>
            <br><br>
        </div>
        <div class="col-md-12" style="margin-bottom: 2em">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-task">
                <thead>
                <tr>
                    <th>{{ trans('message.project_name') }}</th>
                    <th>{{ trans('message.assign_name') }}</th>
                    <th>{{ trans('message.task_name') }}</th>
                    <th>{{ trans('message.done') }}</th>
                    <th>{{ trans('message.due_date') }}</th>
                    <th>{{ trans('message.last_modified_date') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="row bg-top">
        <div class="col-md-12">
            <h2>{{ trans('message.delay_work') }}</h2>
            <br><br>
        </div>
        <div class="col-md-12" style="margin-bottom: 2em">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-due-date">
                <thead>
                <tr>
                    <th>{{ trans('message.project_name') }}</th>
                    <th>{{ trans('message.assign_name') }}</th>
                    <th>{{ trans('message.task_name') }}</th>
                    <th>{{ trans('message.done') }}</th>
                    <th>{{ trans('message.due_date') }}</th>
                    <th>{{ trans('message.last_modified_date') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <script src="{{asset('assets/js/chart.js')}}"></script>
    <script src="{{asset('assets/js/task-dashboard.js')}}"></script>

@endsection