<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.12
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="{{ config('app.locale') }}">
<head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title> PMS - Hanbisoft </title>
    <!-- Icons-->
    <link href="{{asset('vendor/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/flag-icon.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/simple-line-icons.css')}}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{asset('coreui/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/pace-progress/css/pace.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css"
          rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/fontawesome.min.css')}}">
    <script src="{{asset('js/fontawesome.min.js')}}"></script>
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        // Shared ID
        gtag('config', 'UA-118965717-3');
        // Bootstrap ID
        gtag('config', 'UA-118965717-5');
    </script>
    <!-- Custom fonts for this template-->
    <!-- Page level plugin CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/admin-dashboard">
        <img class="navbar-brand-full" src="{{asset('images/logo.png')}}" width="89" height="25" alt="Hanbisoft">

    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown" style="margin-right: 2em !important;"><a
                    href="{!! route('user.change-language', ['en']) !!}"><img
                        src="{{asset('images/en.jpg')}}" alt=""></a>
            <a href="{!! route('user.change-language', ['ko']) !!}"><img
                        src="{{asset('images/ko1.jpg')}}" alt=""></a>
            <a href="{!! route('user.change-language', ['vi']) !!}"><img
                        src="{{asset('images/vi.jpg')}}" alt=""></a>
        </li>
{{--        @if(Auth::user()->is_Admin === 1)--}}
{{--            @if (\Route::current()->getName() != 'project.show')--}}
{{--                <li class="nav-item dropdown" style="margin-right: 2em !important;">--}}
{{--                    <a class="btn btn-info btn-register" href="{{route('calendar-admin-task')}}"--}}
{{--                       target="_top">{{ trans('message.new_task') }}</a>--}}
{{--                </li>--}}
{{--            @endif--}}
{{--        @endif--}}

        @if(Auth::user()->is_Admin === 1)
            @if (\Route::current()->getName() == 'project.show')
                <button class="btn btn-info btn-register" onclick="showModalCreate()">{{ trans('message.new_task') }}</button>
            @endif
        @else
            @if (\Route::current()->getName() == 'project.show')
                <button class="btn btn-info btn-register" onclick="memberCreateTask()">{{ trans('message.new_task') }}</button>
            @endif
        @endif
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="{{route('admin-dashboard')}}" role="button"
               aria-haspopup="true" aria-expanded="false">
                @if(Auth::user()->avatar)
                    <img class="img-avatar" src="{{asset(Auth::user()->avatar)}}" alt="">
                @else
                    <img class="img-avatar" src="{{asset('uploads/2naD61320775-male-avatar-profile-picture-default-user-avatar-guest-avatar-simply-human-head-vector-illustration-i.jpg')}}" alt="">
                @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                <a class="dropdown-item" href="{{route('user-profile.index')}}">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i> Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">

    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('admin-dashboard')}}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                    </a>

                </li>
                <li class="nav-title">Project</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('project.index')}}">
                        <i class="fab fa-trello"></i> &nbsp; {{ trans('message.project') }}</a>
                </li>
                <li class="nav-title">Task</li>
                <li class="nav-item">
                    @if(Auth::user()->is_Admin === 1)
                        <a class="nav-link" href="{{route('calendar-admin-task')}}">
                            <i class="fas fa-tasks"></i> &nbsp; {{ trans('message.task_assign_me') }}</a>
                    @else
                        <a class="nav-link" href="{{route('show-calendar-my-task')}}">
                            <i class="fas fa-tasks"></i>&nbsp;{{ trans('message.task_assign_me') }}</a>
                    @endif
                </li>
                <li class="nav-item nav-dropdown">
                @if(Auth::user()->is_Admin === 1)
                    <li class="nav-title">Manager</li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle">
                            <i class="fas fa-cogs"></i> &nbsp;Manager Tool</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item"><a class="nav-link" href="{{route('grant-chart.index')}}"> <i
                                            class="fas fa-list"></i> &nbsp; {{ trans('message.gantt_chart') }} </a></li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('calendar-admin-task')}}">
                                    <i class="far fa-calendar-alt"></i> &nbsp; {{ trans('message.calendar_all_task') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('company.index')}}">
                                    <i class="far fa-list-alt"></i> &nbsp; {{ trans('message.company_list') }}</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="{{route('report.index')}}"><i
                                            class="far fa-chart-bar"></i> &nbsp; {{ trans('message.report') }} </a></li>
                            <li class="nav-item"><a class="nav-link" href="{{route('admin-user.index')}}"><i
                                            class="fas fa-user-circle"></i> &nbsp; {{ trans('message.user_list') }} </a>
                            </li>

                        </ul>
                    </li>
                @endif

            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
        <!-- Breadcrumb-->
        <br>
        <div class="container-fluid">
            <div class="animated fadeIn">
                {{--                {{ Config::get('app.locale') }}--}}
                @yield('content')
            </div>
        </div>
    </main>
    <aside class="aside-menu">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">
                    <i class="icon-list"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#messages" role="tab">
                    <i class="icon-speech"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#settings" role="tab">
                    <i class="icon-settings"></i>
                </a>
            </li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
            <div class="tab-pane active" id="timeline" role="tabpanel">

            </div>
        </div>
    </aside>
</div>
<footer class="app-footer">
    <div>
        <a href="https://hanbisoft.com">Hanbisoft</a>
        <span>&copy; 2019 </span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://hanbisoft.com">Hanbisoft</a>
    </div>
</footer>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- CoreUI and necessary plugins-->
{{--<script src="vendor/jquery.min.js"></script>--}}
<script src="{{asset('vendor/popper.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/pace.min.js')}}"></script>
<script src="{{asset('vendor/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('vendor/coreui.min.js')}}"></script>
</body>
</html>
