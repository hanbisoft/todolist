@extends('admin.master')

@section('content')

@if(session()->has('message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span>
        <b> {{ session()->get('message') }} </b></span>
</div>
@endif
@foreach ($tasks as $task)
<a href="/project-task/{{ $task->id }}" class="btn btn-sm btn-dark"><i class="fas fa-backward"></i> Back</a>
@endforeach
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal" action="{{ route("project-task.update", ['id'=>$task->id]) }}"
              method="POST" enctype="multipart/form-data">
        <div class="card ">
            <div class="card-header card-header-rose card-header-icon">
                <h4 class="card-title">Update Task</h4>
            </div>
            <div class="card-body ">
                @foreach ($tasks as $task)
                    @csrf
                    {{ method_field('PUT') }}
                    <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
                    <input type="hidden" name="username" value="{{ $task->username }}">
                    <div class="row">
                        <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Task
                            name</label>
                        <div class="col-md-6">
                            <div class="form-group has-default bmd-form-group">
                                @if(Auth::user()->is_Admin === 1)
                                <input type="text" class="form-control" name="tSubject" placeholder="Enter Task name"
                                    required value="{{ $task->tSubject }}">
                                @else
                                    <input readonly type="text" class="form-control" name="tSubject" placeholder="Enter Task name"
                                    required value="{{ $task->tSubject }}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label"><strong class="text-danger">*</strong> Project</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="hidden" value="{{ $task->p_code }}" name="p_code">
                                    <input class="form-control" value="{{ $task->pName }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label"> Assign to</label>
                            <div class="">
                                <select name="assign_to" id="" class="form-control" required>
                                    <option value="{{ $task->assign_to }}">{{ $task->u_nickname }}</option>
                                    @foreach ($user as $item)
                                    <option value="{{ $item->id }}">{{ $item->u_nickname }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label">% Done</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    <select class="form-control" name="t_Done" id="t_Done">
                                        <option selected value="{{ $task->t_Done }}">{{ $task->t_Done }} %</option>
                                        <option value="0">0 %</option>
                                        <option value="10">10 %</option>
                                        <option value="20">20 %</option>
                                        <option value="30">30 %</option>
                                        <option value="40">40 %</option>
                                        <option value="50">50 %</option>
                                        <option value="60">60 %</option>
                                        <option value="70">70 %</option>
                                        <option value="80">80 %</option>
                                        <option value="90">90 %</option>
                                        <option value="100">100 %</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label">Process</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    <select name="t_Process" id="" class="form-control">
                                        @switch($task->t_Process)
                                        @case(1)
                                        <option value="1">New</option>
                                        @break
                                        @case(2)
                                        <option value="2">In Progress</option>
                                        @break
                                        @case(3)
                                        <option value="3">Resolved</option>
                                        @break
                                        @case(4)
                                        <option value="4">Done</option>
                                        @break
                                        @case(5)
                                        <option value="5">Closed</option>
                                        @break
                                        @endswitch
                                        <option value="1">New</option>
                                        <option value="2">In Progress</option>
                                        <option value="3">Resolved</option>
                                        <option value="4">Done</option>
                                        <option value="5">Closed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <br>
                    @if(Auth::user()->is_Admin === 1)
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="t_sDate" id="psDate" required
                                        value="{{ $task->t_sDate }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="">

                                <div class="form-group has-default bmd-form-group">
                                    <label class=" col-form-label"><strong class="text-danger">*</strong> To</label>
                                    <input type="text" class="form-control" name="t_eDate" id="peDate"  value="{{ $task->t_eDate }}"
                                        required>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="t_sDate"  id="psDate" required
                                        value="{{ $task->t_sDate }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="">

                                <div class="form-group has-default bmd-form-group">
                                    <label class=" col-form-label"><strong class="text-danger">*</strong> To</label>
                                    <input type="text" class="form-control" name="t_eDate" id="peDate" value="{{ $task->t_eDate }}"
                                        required readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-form-label"><strong class="text-danger">*</strong> Level</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    @if(Auth::user()->is_Admin === 1)
                                    <select name="t_diff" id="" class="form-control" required>
                                        @switch($task->t_diff)
                                            @case(1)
                                            <option value="1">1</option>
                                            @break
                                            @case(2)
                                            <option value="2">2</option>
                                            @break
                                            @case(3)
                                            <option value="3">3</option>
                                            @break
                                            @case(4)
                                            <option value="4">4</option>
                                            @break
                                            @case(5)
                                            <option value="5">5</option>
                                            @break
                                        @endswitch
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                    </select>
                                    @else
                                    <select name="t_diff" id="" class="form-control" required readonly>
                                        @switch($task->t_diff)
                                            @case(1)
                                            <option value="1">Very difficult</option>
                                            @break
                                            @case(2)
                                            <option value="2">Difficult</option>
                                            @break
                                            @case(3)
                                            <option value="3">Normal</option>
                                            @break
                                            @case(4)
                                            <option value="4">Easy</option>
                                            @break
                                            @case(5)
                                            <option value="5">Basic</option>
                                            @break
                                        @endswitch
                                    </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-form-label"><strong class="text-danger">*</strong> Task Type</label>
                            <div class="">
                                <div class="form-group has-default bmd-form-group">
                                    @if(Auth::user()->is_Admin === 1)
                                    <select name="t_Type" id="" class="form-control" required>
                                        @switch($task->t_Type)
                                        @case(1)
                                        <option value="1" selected readonly>Bug</option>
                                        @break
                                        @case(2)
                                        <option value="2">Development</option>
                                        @break
                                        @case(3)
                                        <option value="3">Unit Testing</option>
                                        @break
                                        @case(4)
                                        <option value="4">Update</option>
                                        @break
                                        @case(5)
                                        <option value="5">Other</option>
                                        @break
                                        @endswitch
                                        <option value="1">Bug</option>
                                        <option value="2">Development</option>
                                        <option value="3">Unit Testing</option>
                                        <option value="4">Update</option>
                                        <option value="5">Other</option>
                                    </select>
                                    @else
                                    <select name="t_Type" id="" class="form-control" required readonly>
                                        @switch($task->t_Type)
                                        @case(1)
                                        <option value="1" selected readonly>Bug</option>
                                        @break
                                        @case(2)
                                        <option value="2">Development</option>
                                        @break
                                        @case(3)
                                        <option value="3">Unit Testing</option>
                                        @break
                                        @case(4)
                                        <option value="4">Update</option>
                                        @break
                                        @case(5)
                                        <option value="5">Other</option>
                                        @break
                                        @endswitch
                                    </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <hr>
                    @if(Auth::user()->is_Admin === 1)
                    <div class="row">
                        <div class="col-md-12">
                            <label class="col-form-label">Content</label>
                            <textarea class="form-control" name="t_Contents" id="t_Contents" cols="30" rows="1"
                                placeholder="Enter content..."> {{ $task->t_Contents }}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="col-md-3 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                        <div class="col-md-9">
                            <div>
                                <input type="file"
                                    accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                    class="form-control" name="a_code" id="a_code" />
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-md-12">
                            <label class="col-form-label">Content</label>
                            <input readonly class="form-control" name="t_Contents" id="t_Contents"
                                placeholder="Enter content..." value="{{ $task->t_Contents }}">
                        </div>
                    </div>

                    @endif
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <h4><strong class="text-danger">*</strong> Notes</h4>
                            <textarea name="task_commit" class="form-control" name="" id="" cols="30" rows="10"
                                placeholder="If you change, please type detail here..." required></textarea>
                        </div>
                    </div>
                    <br>

            </div>
            <div class="card-footer ">
                <div class="row">
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-primary">Update Task</button>
                    </div>
                </div>
            </div>

        </div>
        </form>
@endforeach
</div>
</div>
<script>
    var values = $('#select-meal-type').val();

    function showAssign() {
        $('#assignModal').modal('show')
    }

    $( function() {
        $( "#psDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
        $( "#peDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    } );
</script>
<script>
    CKEDITOR.replace('t_Contents',{
        uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
        filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
        filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
        filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
        filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
        filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
        filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
    });
    var expanded = false;
</script>
@endsection
