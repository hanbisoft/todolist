@extends('admin.master')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src='{{asset('../assets/js/admin.js')}}'></script>
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active " href="{{route('admin-task.index')}}">Task List</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-danger" href="{{route('calendar-admin-task')}}">Calendar</a>
            </li>
        </ul>

    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
            @if(Auth::user()->is_Admin === 1)
            <a class="btn btn-info" href="{{route('calendar-admin-task')}}">New Task</a>
            @endif
    </div>
     <br>
     <br>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Task List</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-task">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Title</th>
                            <th>Created</th>
                            <th>Assign to</th>
                            <th>Date</th>
                            <th>To</th>
                            <th>% Done</th>
                            <th>Level</th>
                            <th>Process</th>
                            <th>Task type</th>
                            <th>Project name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{--    //modal--}}
<div class="modal" tabindex="-1" role="dialog" id="Task_detail">
    <div class="modal-dialog modal-xl" id="Task_detail_closed" role="document">
        <div class="modal-content">
            <form action="{{route('update-task')}}" method="post">
                @csrf
                <input type="hidden" name="id" id="id" value="">
                <div class="modal-header">
                    <div class="col-md-10">
                        <h5 class="modal-title" id="modal-title">Task -  </h5>
                    </div>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="wait2"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <nav>
                                <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                       href="#nav-home" role="tab" aria-controls="nav-home"
                                       aria-selected="true"><strong class="text-justify">Task</strong></a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                       href="#nav-profile" role="tab" aria-controls="nav-profile"
                                       aria-selected="false"><strong class="text-justify">SubTasks</strong></a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                       href="#nav-contact" role="tab" aria-controls="nav-contact"
                                       aria-selected="false"><strong class="text-justify">Task Logs</strong></a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                     aria-labelledby="nav-home-tab">
                                    <table class="table" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Process</th>
                                            <th>Task type</th>
                                            <th>% Done</th>
                                            <th>Project name</th>
                                            <th>Assign</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><span id="title"></span></td>
                                            <td id="process"></td>
                                            <td id="t_task_type"></td>
                                            <td id="tabel_done"></td>
                                            <td id="project_name"></td>
                                            <td id="assign_name"></td>
                                            <td id="url"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <br>
                                    <h3># Update Task</h3>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Assign to</label>
                                                <select name="assign_to" id="assign_user" class="form-control">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for=""> % Done</label>
                                                <select name="t_Done" id="t_Done" class="form-control">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="">Process</label>
                                                <select name="t_Process" id="t_Process" class="form-control"
                                                        required>

                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for=""><strong class="text-danger">*</strong> Note</label>
                                                <textarea name="task_commit" id="" class="form-control" cols="30"
                                                          rows="10" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        @if(Auth::user()->is_Admin === 1)
                                            <div class="col-md-6">
                                                <a  id="delete" style="margin-left: 1em"></a> <a  id="create_sub_task" style="float: left;text-align: right;"></a>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        @else
                                            <div class="col-md-12 text-right">
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        @endif
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                     aria-labelledby="nav-profile-tab">

                                    <table class="table" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Date</th>
                                            <th>To</th>
                                            <th>Assign</th>
                                            <th>% Done</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="sub_task">

                                        </tbody>
                                    </table>
                                    <br>
                                    <hr>

                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                     aria-labelledby="nav-contact-tab">
                                    <ul class="list-group" id="logs">

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">


                </div>
            </form>
        </div>
    </div>

</div>
<script>
    function confirmDelete($id) {
        if (confirm("Are you sure you want to delete this item?") == true) {
            $(document).find('#frm_delete_' + $id).submit();
        } else {
            return false
        }
    }
</script>
@endsection
