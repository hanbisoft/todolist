@extends('admin.master')

@section('content')
    <br>
    @foreach ($task as $value)
        <div class="row">
            <div class="col-md-8">
                <h1>#{{$value->id}} {{ $value->tSubject }}</h1>
                <p class="">{!! $value->t_Contents !!}</p>
            </div>
            <div class="col-md-4 text-center">
                @if(Auth::user()->is_Admin === 1 || Auth::user()->id === $value->assign_to)
                    <a href="/project-task/{{ $value->id }}/edit" rel="tooltip" class="btn btn-success"
                       data-original-title="" title="">
                        <i class="far fa-edit"></i>
                    </a>
                @endif
            </div>
        </div>
    @endforeach
    <hr>
    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Task Detail</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>To</th>
                            <th>Assignee</th>
                            <th>Process</th>
                            <th>% Done</th>
                            <th>Task type</th>
                            <th>Level</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($task as $value)
                            <tr>
                                <td> {{ $value->t_sDate }}</td>
                                <td> {{ $value->t_eDate }}</td>
                                <td> {{ $value->username }}</td>
                                <td>
                                    @switch($value->t_Process)
                                        @case(1)
                                        <button class=" btn btn-sm btn-primary">New</button>
                                        @break
                                        @case(2)
                                        <button class="btn-sm btn-info">In Progress</button>
                                        @break
                                        @case(3)
                                        <button class="btn btn-sm btn-warning">Resolved</button>
                                        @break
                                        @case(4)
                                        <button class="btn btn-sm btn-success"> Done</button>
                                        @break
                                        @case(5)
                                        <button disabled class=" btn btn-sm btn-secondary">Closed</button>
                                        @break
                                    @endswitch
                                </td>
                                <td>   <div class="custom_done" style='background-size:{{$value->t_Done}}px 100px'> {{$value->t_Done}}%</div>
                                <td>
                                    @switch($value->t_Type)
                                        @case(1)

                                        <button disabled type="button" class="btn btn-sm btn-danger">Bug</button>
                                        @break
                                        @case(2)

                                        <button disabled type="button" class="btn btn-sm btn-primary">Development
                                        </button>
                                        @break
                                        @case(3)

                                        <button disabled type="button" class="btn btn-sm btn-info"> Unit Testing
                                        </button>
                                        @break
                                        @case(4)
                                        <button disabled type="button" class="btn btn-sm btn-warning">Update</button>

                                        @break
                                        @case(5)
                                        <button disabled type="button" class="btn btn-sm btn-link">Other</button>

                                        @break
                                        @default

                                    @endswitch
                                </td>
                                <td>
                                    @switch($value->t_diff)
                                        @case(1)
                                        Easy
                                        @break
                                        @case(2)
                                        Difficult
                                        @break
                                        @case(3)
                                        Very difficult
                                        @break
                                    @endswitch
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Attachments</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-info">
                        <th>ID</th>
                        <th>Name</th>
                        <th colspan="2">Action</th>
                        </thead>
                        <tbody>
                        @foreach ($attachment as $at)
                            <tr>
                                <td>{{ $at->id }}</td>
                                <td>{{str_replace("public/documents/","",$at->a_fileName)}}</td>
                                <td>
                                    <form action="{{ route('download') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="url" value="{{ $at->a_file }}">
                                        <button class="btn btn-sm btn-link" type="submit">Download</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                </div>
            </div>
            <br>
        </div>
    </div>
    <hr>
    <div class="row">

    </div>

    <div class="row">
        <div class="col-md-7">
            <div class="card">
                <div class="card-header card-header-success">
                    <h4 class="card-title"># SubTasks</h4>
                </div>
                <div class="card-body table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Task Name</th>
                    <th>Date</th>
                    <th>To</th>
                    <th>Assignee</th>
                    <th>Process</th>
                    <th>% Done</th>
                    <th>Task type</th>
                    <th>Level</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($sub_task as $value)
                    <tr>
                        <td>{{ $value->tSubject }}</td>
                        <td> {{ $value->t_sDate }}</td>
                        <td> {{ $value->t_eDate }}</td>
                        <td> {{ $value->username }}</td>
                        <td>
                            @switch($value->t_Process)
                                @case(1)
                                <button class=" btn btn-sm btn-primary">New</button>
                                @break
                                @case(2)
                                <button class="btn-sm btn-info">In Progress</button>
                                @break
                                @case(3)
                                <button class="btn btn-sm btn-warning">Resolved</button>
                                @break
                                @case(4)
                                <button class="btn btn-sm btn-success"> Done</button>
                                @break
                                @case(5)
                                <button disabled class=" btn btn-sm btn-secondary">Closed</button>
                                @break
                            @endswitch
                        </td>
                        <td>  <div class="custom_done" style='background-size:{{$value->t_Done}}px 100px'> {{$value->t_Done}}%</div></td>
                        <td>
                            @switch($value->t_Type)
                                @case(1)

                                <button disabled type="button" class="btn btn-sm btn-danger">Bug</button>
                                @break
                                @case(2)

                                <button disabled type="button" class="btn btn-sm btn-primary">Development
                                </button>
                                @break
                                @case(3)

                                <button disabled type="button" class="btn btn-sm btn-info"> Unit Testing
                                </button>
                                @break
                                @case(4)
                                <button disabled type="button" class="btn btn-sm btn-warning">Update</button>

                                @break
                                @case(5)
                                <button disabled type="button" class="btn btn-sm btn-link">Other</button>

                                @break
                                @default

                            @endswitch
                        </td>
                        <td>
                            @switch($value->t_diff)
                                @case(1)
                                Easy
                                @break
                                @case(2)
                                Difficult
                                @break
                                @case(3)
                                Very difficult
                                @break
                            @endswitch
                        </td>
                        <td><a class="btn btn-info btn-round" href="/project-task/{{  $value->id }} "> <i
                                        class="fas fa-eye"></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
            </div>
        </div>
        <div class="col-md-5">
            <h4 class="text-info"># Task Logs</h4>
            <br>
            <ul class="list-group">
                @foreach ($log as $item)
                    <li class="list-group-item">{!! $item->l_status !!} <br>{{ $item->l_content }}</li>
                @endforeach
            </ul>

        </div>
    </div>
    <hr>
    @foreach ($task as $value)
        <a href="/project/{{ $value->p_id }}" class="btn btn-sm btn-dark"><i class="fas fa-backward"></i> Back</a>
    @endforeach
    <br>
    <br>
@endsection
