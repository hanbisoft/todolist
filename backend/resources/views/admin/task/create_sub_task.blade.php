@extends('admin.master')

@section('content')
    <link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}">
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>
        <b> {{ session()->get('message') }} </b></span>
        </div>
    @endif
    <div class="row">
        <form class="form-horizontal" action="{{ route("createMemberSubTask") }}" method="POST"
              enctype="multipart/form-data" style="width: 100%">
            <input type="hidden" name="p_code" class="form-control" value="{{ $task->p_code }}">
            <input type="hidden" name="t_code" class="form-control" value="{{ $task->id }}">
            <input type="hidden" name="p_code" class="form-control" value="{{ $task->p_code }}">
            <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
            <input type="hidden" name="parent_id" value="{{ $task->id }}">
            @csrf
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header card-header-rose card-header-icon">
                        <h4 class="card-title">Create Sub Task</h4>
                    </div>
                    <div class="card-body ">
                        <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
                        <input type="hidden" name="t_Done" value="0">
                        <input id="project_create" class="form-control" type="hidden" readonly
                               value="{{ $task->p_code }}">
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Task
                                name</label>
                            <div class="col-md-6">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="tSubject"
                                           placeholder="Enter Task name"
                                           required>
                                </div>
                            </div>
                            <input type="hidden" name="t_Done">

                        </div>
                        <br>
                        <div class="row">
                                <label class="col-md-3 col-form-label">Assign to</label>
                                <div class="col-md-6">
                                    <select class="custom-select" name="arrUser[]" id="basic" multiple="multiple" required>
                                        @foreach($user as $value)
                                            <option value="{{$value->user_id}}">{{$value->u_nickname}}</option>
                                        @endforeach
                                    </select>
                                    <span id="waiting"></span>
                                </div>
                        </div>

                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <input type="text" class="form-control" name="t_sDate" id="start_date"
                                               placeholder="Select date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <label class=" col-form-label"><strong class="text-danger">*</strong> To</label>
                                        <input type="text" class="form-control" name="t_eDate" id="end_date"
                                               placeholder="Select to" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Level</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_diff" id="" class="form-control" required>
                                            <option selected value="1">Easy</option>
                                            <option value="2">Difficult</option>
                                            <option value="3">Very difficult</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Task Type</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_Type" id="" class="form-control" required>
                                            <option value="1">Bug</option>
                                            <option value="2">Development</option>
                                            <option value="3">Unit Testing</option>
                                            <option value="4">Update</option>
                                            <option value="5">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label">Previous Task</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_Pred" id="" class="form-control">
                                            <option value="">None</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Process</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_Process" id="" class="form-control" required>
                                            <option value="1">New</option>
                                            <option value="2">In Progress</option>
                                            <option value="3">Resolved</option>
                                            <option value="4">Done</option>
                                            <option value="5">Closed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-form-label">Content</label>
                                <textarea class="form-control" name="t_Contents" id="t_Contents" cols="30" rows="10"
                                          placeholder="Enter content..."></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                            <div class="col-md-9">
                                <div>
                                    <input type="file"
                                           accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                           class="form-control" name="a_code" id="a_code"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary">Create</button>
                                <a href="{{route('calendar-admin-task')}}" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  //modal user  --}}

            </div>
        </form>
    </div>
    <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
    <script src='{{asset('../assets/js/input-date.js')}}'></script>
    <script>
        $('#basic').multiselect({
            templates: {
                li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
            }
        });

        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2') ;
            }
            return val+ ' '+ '$';
        }
        $('#pAmount').focusout(function(){

            $("#pAmount_2").val(
                commaSeparateNumber($(this).val())
            );
        });
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };
        CKEDITOR.replace('t_Contents', options);
    </script>
@endsection
