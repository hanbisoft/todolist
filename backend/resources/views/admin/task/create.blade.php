@extends('admin.master')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>
        <b> {{ session()->get('message') }} </b></span>
        </div>
    @endif
    <div class="row">
        <form class="form-horizontal" action="{{ route("project-task.store") }}" method="POST"
              enctype="multipart/form-data" style="width: 100%">
            @csrf
            <input type="hidden" name="calendar" value="form">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header card-header-rose card-header-icon">
                        <h4 class="card-title">Create Task</h4>
                    </div>
                    <div class="card-body ">
                        <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
                        <input type="hidden" name="t_Done" value="0">
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Task
                                name</label>
                            <div class="col-md-6">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="tSubject"
                                           placeholder="Enter Task name"
                                           required>
                                </div>
                            </div>
                            <input type="hidden" name="t_Done">

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Project</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="p_code" id="project_create" class="form-control" onchange="showUserProject()" required>
                                            <option value="0">Please select project</option>
                                            @foreach ($project as $item)
                                                <option value="{{ $item->id }}">{{ $item->pName }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label">Assign to</label>
                                <div>
{{--                                    <button onclick="showAssign()" type="button" class="btn  btn-info">Add member</button>--}}
                                <select name="arrUser[]" id="arrUser" class="form-control"><option value="">Please select user</option></select>
                                <span id="waiting" ></span>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <input type="text" class="form-control" name="t_sDate" id="start_date" placeholder="Select date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <label class=" col-form-label"><strong class="text-danger">*</strong> To</label>
                                        <input type="text" class="form-control" name="t_eDate" id="end_date"
                                               placeholder="Select to" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Level</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_diff" id="" class="form-control" required>
                                            <option value="1">Very difficult</option>
                                            <option value="2">Difficult</option>
                                            <option value="3">Normal</option>
                                            <option value="4">Easy</option>
                                            <option value="5">Basic</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Task Type</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_Type" id="" class="form-control" required>
                                            <option value="">Please select task type</option>
                                            <option value="1">Bug</option>
                                            <option value="2">Development</option>
                                            <option value="3">Unit Testing</option>
                                            <option value="4">Update</option>
                                            <option value="5">Other</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="col-form-label">Previous Task</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_Pred" id="" class="form-control">
                                            <option value="">None</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="col-form-label"><strong class="text-danger">*</strong> Process</label>
                                <div class="">
                                    <div class="form-group has-default bmd-form-group">
                                        <select name="t_Process" id="" class="form-control" required>
                                            <option value="1">New</option>
                                            <option value="2">In Progress</option>
                                            <option value="3">Resolved</option>
                                            <option value="4">Done</option>
                                            <option value="5">Closed</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-form-label">Content</label>
                                <textarea class="form-control" name="t_Contents" id="t_Contents" cols="30" rows="10"
                                          placeholder="Enter content..."></textarea>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                            <div class="col-md-9">
                                <div>
                                    <input type="file"
                                           accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                           class="form-control" name="a_code" id="a_code"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  //modal user  --}}
                <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true" id="assignModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Assign member</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-left">
                                    <ul class="list-group">
                                        @foreach ($user as $item)
                                        <li class="list-group-item" style="padding-left: 2em">
                                            <input name="arrUser[]" class="form-check-input" type="checkbox"  value="{{ $item->id }}"> {{ $item->name }} - {{ $item->email }}
                                        </li>
                                        @endforeach
                                    </ul>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        var values = $('#select-meal-type').val();

        function showAssign() {
            $('#assignModal').modal('show')

        }

        $(function () {
            $("#start_date").datepicker({
                dateFormat: "yy-mm-dd"
            });
            $("#end_date").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

        function showUserProject()
        {
            var id = document.getElementById('project_create').value;
            console.log(id)
            $.ajax({
                url: "{{ route('show-all-user') }}",
                type: "get",
                beforeSend: function () {
                    $('#arrUser').hide();
                    $('#waiting').text("Loading....")
                },
                complete: function () {
                    $('#waiting').hide()
                    $('#arrUser').show();

                },
                dateType: "json",
                data: {
                    'id':id
                },
                success: function (result) {
                    console.log(result.code)
                    if(result.code === 200)
                    {
                        resultJson = result.data;
                        $('#arrUser').children('option:not(:first)').remove();
                        Object.keys(resultJson).forEach(function(key) {
                            var mySelect = document.getElementById('arrUser'),
                                newOption = document.createElement('option');
                            newOption.value = resultJson[key].user_id;
                            newOption.innerText = resultJson[key].username;
                            mySelect.appendChild(newOption);
                        })
                    }else {
                        $('#arrUser').children('option:not(:first)').remove();
                    }
                }
            });
        }
    </script>
    <script>
        CKEDITOR.replace('t_Contents',{
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        var expanded = false;
    </script>
@endsection
