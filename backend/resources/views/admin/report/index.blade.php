@extends('admin.master')

@section('content')
<link rel="stylesheet" href="{{asset('css/chartist.min.css')}}">
<script src="{{asset('scripts/chartist.min.js')}}"></script>
<script src="{{ asset('assets/js/input-date.js') }}"></script>

<div class="container-fuild bg-top" style="min-height: 800px">
  <div class="container">
        <div id="wait"></div>
        <div class="row ">
            <div class="col-md-12" style="padding-left:2em;margin-bottom:2em">
                 <h1>Report</h1>
            </div>
            <div class="col-md-12">
               
             <form class="form-inline">
                 <div class="form-group mx-sm-3 mb-2">
                         <label> Date:</label>
                 </div>
                 <div class="form-group mx-sm-3 mb-2">
                   <label for="inputPassword2" class="sr-only">Date</label>
                   <input type="text" class="form-control" id="start_date" placeholder="date">
                 </div>
                 <div class="form-group mx-sm-3 mb-2">
                   <label for="" class="sr-only">To</label>
                   <input type="text" class="form-control" id="end_date" placeholder="to" >
                 </div>
                 <div class="form-group mx-sm-3 mb-2">
                        <label> Project:</label>
                </div>
                 <div class="form-group mx-sm-3 mb-2">
                     <select name="id" id="id" class="form-control" required>
                         @foreach ($project as $item)
                             <option value="{{ $item->id }}">{{ $item->pName }}</option>
                         @endforeach
                        
                     </select>
                 </div>
                 <button type="button" onclick="callApi()" class="btn btn-info mb-2">Search</button>
               </form>
               
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-4" id="project">
                <div ></div>
                <div  id="pie"></div>
            </div>
            <div class="col-md-8" style="padding-left: 20%;">
             <table class=" table table-striped">
                 <thead>
                 <tr>
                     <th scope="col">Task</th>
                     <th scope="col">Count</th>
                 </tr>
                 </thead>
                  <tbody>
                      <tr>
                          <th>Total Task</th>
                          <td id="total"> </td>
                      </tr>
                      <tr>
                          <th>Point</th>
                          <td id="point"></td>
                      </tr>
                      <tr>
                          <th>Done</th>
                          <td id="done"> </td>
                      </tr>
                      <tr>
                          <th>Delay</th>
                          <td id="delay"></td>
                      </tr>
                      <tr>
                          <th>Remainder</th>
                          <td id="remainder"></td>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>
        <br><br>
        <div class="row">
                <div class="col-md-12">
                      <table class="table">
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Name</th>
                                      <th>Task</th>
                                      <th>Level</th>
                                      <th>Process</th>
                                      <th>Due date</th>
                                  </tr>
                              </thead>
                              <tbody id="task">
                                  
                              </tbody>
                          </table>
                </div>
              </div>
        
  </div>
</div>
<script src="{{ asset('assets/js/report.js') }}"></script>
@endsection