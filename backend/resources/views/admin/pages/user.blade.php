@extends('admin.master')
@section('title')
    User Profile
@endsection
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">Update Profile</h4>
                            <p class="card-category">Complete your profile</p>
                        </div>

                        <div class="card-body">
                            <form action="{{route('user-profile.update', ['id' => $user->id])}}"
                                  enctype="multipart/form-data" method="POST">
                                @csrf
                                {{ method_field('PUT') }}
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Name</label>
                                            <input value="{{ $user->name }}" name="name" type="text"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Email address</label>
                                            <input type="email" value="{{ $user->email }}" name="email"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Phone</label>
                                            <input value="{{ $user->u_phone }}" type="number" name="u_phone"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Cost</label></label>
                                            <input value="{{ $user->u_cost }}" type="number" name="u_cost"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Nick name</label></label>
                                            <input value="{{ $user->u_nickname }}" type="text" name="u_nickname"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Color</label></label>
                                            <input value="{{ $user->u_color }}" type="color" name="u_color"
                                                   class="form-control">
                                        </div>
                                    </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Avatar</label></label>
                                                <input  accept="image/gif, image/jpg, image/jpeg, image/png"  name="img"  value="{{ $user->avatar }}" type="file" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                                    <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="#pablo">
                                @if(Auth::user()->avatar)
                                <img width="100%" height="400" class="img"  src="{{asset($user->avatar)}}"/>
                                @else
                                    <img width="100%" height="400" class="img"  src="uploads/2naD61320775-male-avatar-profile-picture-default-user-avatar-guest-avatar-simply-human-head-vector-illustration-i.jpg"/>
                                @endif

                            </a>
                        </div>
                        <div class="card-body">
                            <h6 class="card-category text-gray">{{ $user->name }}</h6>
                            <h4 class="card-title">{{ $user->u_nickname }}</h4>
                            <p class="card-description">
                                {{ $user->email }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
