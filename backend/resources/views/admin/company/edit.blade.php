@extends('admin.master')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>{{ session()->get('message') }}</span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <h4 class="card-title">Update Company</h4>
                </div>
                <div class="card-body ">
                    <form class="form-horizontal" action="{{ route("company.update",['id'=>$company->id]) }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Company name</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="c_name" value="{{$company->c_name}}" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-3 col-form-label">Tel</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="c_tel" value="{{$company->c_tel}}" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-3 col-form-label">Email</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="email" class="form-control" name="c_email" value="{{$company->c_email}}">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
