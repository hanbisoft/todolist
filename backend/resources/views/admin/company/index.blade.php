@extends('admin.master')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>
        {{ session()->get('message') }}</span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
                <a class="btn btn-info" href="{{ route('company.create') }}">New Company</a>
        </div>
        <br>
        <br>
        <br>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Company List</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-task">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name </th>
                                <th>Tel</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function confirmDelete($id) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $(document).find('#frm_delete_' + $id).submit();
            } else {
                return false
            }
        }
        $(function () {
            $('#dataTables-task').DataTable({
                async: true,
                ajax: {
                    "url": "{{ route('show-data-company') }}",
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [
                    {data: 'id',orderable: true},
                    {data: 'c_name',searchable: true},
                    {data: 'c_tel',orderable: false, searchable: false},
                    {data: 'c_email',orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false,class:"text-center"},
                ]
            });
        });
    </script>
@endsection
