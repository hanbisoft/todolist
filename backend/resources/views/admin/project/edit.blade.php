@extends('admin.master')

@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>
        {{ session()->get('message') }}</span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">

                    <h4 class="card-title">Update Project</h4>
                </div>
                <div class="card-body ">
                    <form class="form-horizontal" action="{{ route("project.update",['id'=>$project->id]) }}"
                          method="POST"
                          enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        @csrf
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Project
                                name</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="pName" class="form-control" name="pName"
                                           placeholder="Enter Project name"
                                           required value="{{ $project->pName }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Project
                                Type</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                    <select class="form-control" name="pType" id="pType" required>
                                        @if($project->pType === 'large')
                                            <option value="large">Large Project</option>
                                            <option value="small">Small Project</option>
                                        @else
                                            <option value="small">Small Project</option>
                                            <option value="large">Large Project</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong>&nbsp;</strong> Contents</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                <textarea class="form-control text-left" name="pContents" id="pContents" cols="30"
                                          rows="10" placeholder="Enter Content...">
                                    {{ $project->pContents }}
                                </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Amount</label>
                            <div class="col-md-9">
                                <div class="form-group has-default bmd-form-group">
                                    <input type="number" class="form-control" name="pAmount" id="pAmount" maxlength="10"  oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" value="{{ $project->pAmount }}"/>
                                </div>
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" value="{{number_format($project->pAmount)}} $" id="pAmount_2" style="padding-left: 1em"
                                           readonly/>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label class=" col-form-label"><strong class="text-danger">*</strong> Date</label>
                                <div class="form-group has-default bmd-form-group">
                                    <input type="text" class="form-control" name="psDate" id="psDate"
                                           placeholder="Select date" value="{{$project->psDate}}" required/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label class=" col-form-label"><strong class="text-danger">*</strong> To</label>
                                <div class="form-group bmd-form-group is-filled">
                                    <input type="text" class="form-control" name="peDate" id="peDate"
                                           placeholder="Select to" value="{{$project->peDate}}" required/>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-md-3 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                            <div class="col-md-9">
                                <div>
                                    <input type="file"
                                           accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                           class="form-control" name="a_code" id="a_code"/>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="card-footer ">
                            <div class="row">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $("#psDate").datepicker({
                dateFormat: "yy-mm-dd"
            });
            $("#peDate").datepicker({
                dateFormat: "yy-mm-dd"
            });
        });

        function commaSeparateNumber(val){
            while (/(\d+)(\d{3})/.test(val.toString())){
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2') ;
            }
            return val+ ' '+ '$';
        }
        $('#pAmount').focusout(function(){

            $("#pAmount_2").val(
                commaSeparateNumber($(this).val())
            );
        });
    </script>
    <script>
        CKEDITOR.replace('pContents',{
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        var expanded = false;
    </script>
@endsection
