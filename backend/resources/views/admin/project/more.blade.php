@extends('admin.master')
@section('title')
    Project
@endsection
@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/chartist.min.css')}}">
    <script src="{{asset('scripts/chartist.min.js')}}"></script>
    <div class="row bg-top" id="pie">
        <div class="col-md-12" style="margin-bottom: 2em">
            <h1> {{ trans('message.project_percent') }}</h1>
        </div>
        <div id="wait"></div>
    </div>
    <script src="{{asset('assets/js/more.js')}}"></script>
@endsection