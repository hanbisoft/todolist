@extends('admin.master')

@section('content')
    <link rel="stylesheet" href="{{asset('css/chartist.min.css')}}">
    <script src="{{asset('scripts/chartist.min.js')}}"></script>
    <script src='{{asset('../assets/js/input-date.js')}}'></script>
@if(session()->has('message'))
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
    </button>
    <span>{{ session()->get('message') }}</span>
</div>
@endif
<div id="wait"></div>
<div class="row bg-top">
    <div class="col-12">
        <h2 class=\"text-center\">{{$project->pName}}</h2>
        <br>
        <p > <strong class="text-info">Start:</strong> {{$project->psDate}} - <strong class="text-info">End:</strong> {{$project->peDate}}</p>
{{--        {!! $project->pContents !!}--}}
</div>
    <div class="col-md-4" id="project">
    <div ></div>
    <div  id="pie"></div>
    </div>
    <div class="col-md-8" style="padding-left: 20%;">
        <input type="hidden" id="project_id" value="{{$project->id}}">
        <table class=" table table-striped">
           <thead>
           <tr>
               <th scope="col">Task</th>
               <th scope="col">Count</th>
           </tr>
           </thead>
            <tbody>
                <tr>
                    <th>Total Task</th>
                    <td id="total"> </td>
                </tr>
                <tr>
                    <th>Point</th>
                    <td id="point"></td>
                </tr>
                <tr>
                    <th>Done</th>
                    <td id="done"> </td>
                </tr>
                <tr>
                    <th>Delay</th>
                    <td id="delay"></td>
                </tr>
                <tr>
                    <th>Remainder</th>
                    <td id="remainder"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
    <div class="row  bg-top">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4>Work (Today)</h4>
                </div>
                <div class="card-body table-responsive">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="text-info">
                            <th>Project</th>
                            <th>Assign</th>
                            <th>Created</th>
                            <th>Task name</th>
                            <th>% Done</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Task type</th>
                            <th>Process</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach ($showTask as $t)
                                    <tr>
                                        <td>{{$project->pName}}</td>
                                        <td>{{ $t->u_nickname }}</td>
                                        <td>{{ $t->created_task_name }}</td>
                                        <td>
                                            @if ($t->t_Process == 5)
                                                <strike>{{ $t->tSubject }}</strike>
                                            @elseif($t->t_Process == 4)
                                                <span class="text-success">{{ $t->tSubject }}</span>
                                            @else
                                                <a href="/project-task/{{$t->id}}"> {{ $t->tSubject }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="custom_done" style='background-size:{{$t->t_Done}}px 100px'> {{$t->t_Done}}%</div>
                                        </td>
                                        <td>{{ $t->t_sDate }}</td>
                                        <td>{{ $t->t_eDate }}</td>
                                        <td>
                                            <form class="form-horizontal"
                                                  action="{{ route("project.update-process-task") }}"
                                                  method="POST" id="frm_update_{{ $t->id }}">
                                                @csrf
                                                <input type="hidden" name="get_id_task" id="get_id_task"
                                                       value="{{ $t->id }}">

                                                @switch($t->t_Process)
                                                    @case(1)
                                                    <span class="text-primary">New</span>
                                                    @break
                                                    @case(2)
                                                    <span class="text-info">In Progress</span>
                                                    @break
                                                    @case(3)
                                                    <span class="text-warning">Resolved</span>
                                                    @break
                                                    @case(4)
                                                    <span class="text-success"> Done</span>
                                                    @break
                                                    @case(5)
                                                    <span class="text-dark">Closed</span>
                                                    @break
                                                @endswitch
                                            </form>
                                        </td>
                                        <td>
                                            @switch($t->t_Type)
                                                @case(1)

                                                <button disabled type="button" class="btn btn-sm btn-danger">Bug
                                                </button>
                                                @break
                                                @case(2)

                                                <button disabled type="button" class="btn btn-sm btn-primary">
                                                    Development
                                                </button>
                                                @break
                                                @case(3)

                                                <button disabled type="button" class="btn btn-sm btn-info"> Unit
                                                    Testing
                                                </button>
                                                @break
                                                @case(4)
                                                <button disabled type="button" class="btn btn-sm btn-warning">Update
                                                </button>

                                                @break
                                                @case(5)
                                                <button disabled type="button" class="btn btn-sm btn-link">Other
                                                </button>

                                                @break
                                                @default

                                            @endswitch
                                        </td>
                                        <td>
                                            <a class="btn btn-sm btn-info btn-round" href="/project-task/{{  $t->id }} "> <i
                                                        class="fas fa-eye"></i></a>
                                            @if(Auth::user()->is_Admin === 1)
                                                <button type="button" rel="tooltip" class="btn btn-sm btn-danger"
                                                        data-original-title="" title=""
                                                        onclick="return confirmDelete({{ $t->id }})">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                                <form action="/project-task/{{ $t->id }}" method="post"
                                                      id="frm_delete_{{  $t->id}}">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{$showTask->links()}}
            </div>
        </div>
    </div>

    <div class="row bg-top">
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header card-header-success">
                    <h4 class="card-title">Team Member</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-info">
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th colspan="2">Action</th>
                        </thead>
                        <tbody>
                        @foreach ($project_item as $item)
                            <tr>
                                <td>{{ $item->u_id }}</td>
                                <td>{{ $item->u_nickname }}</td>
                                <td>{{ $item->email }}</td>
                                <td colspan="2">
                                    @if(Auth::user()->is_Admin === 1)
                                        <button type="button" rel="tooltip" class="btn btn-sm btn-danger btn-register"
                                                data-original-title="" title=""
                                                onclick="return confirmDelete({{ $item->pr_id }})">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                        <form action="/project-item/{{ $item->pr_id }}" method="post"
                                              id="frm_delete_{{  $item->pr_id}}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="_method" value="DELETE">
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    @if(Auth::user()->is_Admin === 1)
                        <a class="btn btn-success btn-register" href="/add-member/{{ $id }}">Add Member<div class="ripple-container"></div>
                        </a>
                    @endif
                </div>
            </div>
            <br>

        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Attachment Project</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-info">
                        <th>ID</th>
                        <th>Name</th>
                        <th colspan="2">Action</th>
                        </thead>
                        <tbody>
                        @foreach ($attachment as $at)
                            <tr>
                                <td>{{ $at->id }}</td>
                                <td>{{str_replace("public/documents/","",$at->a_fileName)}}</td>
                                <td>
                                    <form action="{{ route('download') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="url" value="{{ $at->a_file }}">
                                        <button class="btn btn-sm btn-link" type="submit">Download</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br>
       <div class="container">
           <div class="row">
               <div class="col-md-12 text-left">
                   {!! $project->pContents !!}
               </div>
           </div>
       </div>
    </div>
    {{--    //modal--}}
    <div class="modal" tabindex="-1" role="dialog" id="createTask">

        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Task</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal" action="{{ route("project-task.store") }}" method="POST"
                      enctype="multipart/form-data" style="width: 100%">
                    @csrf
                    <input type="hidden" name="calendar" value="calendar">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
                            <input type="hidden" name="t_Done" value="0">
                            <div class="row">
                                <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Task
                                    name</label>
                                <div class="col-md-6">
                                    <div class="form-group has-default bmd-form-group">
                                        <input type="text" class="form-control" name="tSubject"
                                               placeholder="Enter Task name"
                                               required>
                                    </div>
                                </div>
                                <input type="hidden" name="t_Done">

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Project</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="p_code" id="project_create" class="form-control" readonly="" required>
                                                <option  selected value="{{$project->id}}">{{$project->pName}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    &ensp;&ensp;
                                    <label class="col-form-label">Assign to</label>
                                    <div class="bs-docs-example">
                                        <select class="custom-select" name="arrUser[]" id="basic" multiple="multiple"
                                                style="display: none;">
                                        </select>
                                    </div>
                                    <span id="waiting"></span>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <input type="text" class="form-control" name="t_sDate" id="start_date"
                                                   placeholder="Select date" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="">

                                        <div class="form-group has-default bmd-form-group">
                                            <label class=" col-form-label"><strong class="text-danger">*</strong>
                                                To</label>
                                            <input type="text" class="form-control" name="t_eDate" id="end_date"
                                                   placeholder="Select to" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Level</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_diff" id="" class="form-control" required>
                                                <option selected value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value=5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Task
                                        Type</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Type" id="" class="form-control" required>
                                                <option value="2">Development</option>
                                                <option value="1">Bug</option>
                                                <option value="3">Unit Testing</option>
                                                <option value="4">Update</option>
                                                <option value="5">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Previous Task</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Pred" id="" class="form-control">
                                                <option value="">None</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Process</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Process" id="" class="form-control" required>
                                                <option value="1">New</option>
                                                <option value="2">In Progress</option>
                                                <option value="3">Resolved</option>
                                                <option value="4">Done</option>
                                                <option value="5">Closed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="col-form-label">Content</label>
                                    <textarea class="form-control" name="t_Contents" id="t_Contents" cols="30" rows="10"
                                              placeholder="Enter content..."></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <label class="col-md-4 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                                <div class="col-md-8">
                                    <div>
                                        <input type="file"
                                               accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                               class="form-control" name="a_code" id="a_code"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--  //modal user  --}}

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="createTaskMember">

        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Member Create Task</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal" action="{{ route("project-task.store") }}" method="POST"
                      enctype="multipart/form-data" style="width: 100%">
                    @csrf
                    <input type="hidden" name="calendar" value="calendar">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
                            <input type="hidden" name="t_Done" value="0">
                            <div class="row">
                                <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Task
                                    name</label>
                                <div class="col-md-6">
                                    <div class="form-group has-default bmd-form-group">
                                        <input type="text" class="form-control" name="tSubject"
                                               placeholder="Enter Task name"
                                               required>
                                    </div>
                                </div>
                                <input type="hidden" name="t_Done">

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Project</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="p_code" id="project_create" class="form-control" readonly="" required>
                                                <option  selected value="{{$project->id}}">{{$project->pName}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    &ensp;&ensp;
                                    <label class="col-form-label">Assign to</label>
                                    <div class="bs-docs-example">
                                        <select class="form-control" name="arrUser[]"  readonly="" required>
                                            <option  selected value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
                                        </select>
                                    </div>
                                    <span id="waiting"></span>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <input type="text" class="form-control" name="t_sDate" id="start_date2"
                                                   placeholder="Select date" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="">

                                        <div class="form-group has-default bmd-form-group">
                                            <label class=" col-form-label"><strong class="text-danger">*</strong>
                                                To</label>
                                            <input type="text" class="form-control" name="t_eDate" id="end_date2"
                                                   placeholder="Select to" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Level</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_diff" id="" class="form-control" required>
                                                <option selected value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value=5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Task
                                        Type</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Type" id="" class="form-control" required>
                                                <option value="2">Development</option>
                                                <option value="1">Bug</option>
                                                <option value="3">Unit Testing</option>
                                                <option value="4">Update</option>
                                                <option value="5">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Previous Task</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Pred" id="" class="form-control">
                                                <option value="">None</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Process</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Process" id="" class="form-control" required>
                                                <option value="1">New</option>
                                                <option value="2">In Progress</option>
                                                <option value="3">Resolved</option>
                                                <option value="4">Done</option>
                                                <option value="5">Closed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="col-form-label">Content</label>
                                    <textarea class="form-control" name="t_Contents" id="t_Contents" cols="30" rows="10"
                                              placeholder="Enter content..."></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <label class="col-md-4 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                                <div class="col-md-8">
                                    <div>
                                        <input type="file"
                                               accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                               class="form-control" name="a_code" id="a_code"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--  //modal user  --}}

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
    <script>
        CKEDITOR.replace('t_Contents',{
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        var expanded = false;
    </script>
    <script>
this.showUserProject()
        function confirmDelete($id) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $(document).find('#frm_delete_' + $id).submit();
            } else {
                return false
            }
        }
        function showModalCreate() {
            $('#createTask').modal('show')
        }
        function memberCreateTask() {
            $('#createTaskMember').modal('show')
        }

        function showUserProject() {
            var id = document.getElementById('project_create').value;

            $.ajax({
                url: "/show-all-user",
                type: "get",
                beforeSend: function () {
                    $('#basic').hide();
                    $('#waiting').text("Loading....")
                },
                complete: function () {
                    $('#waiting').hide()
                    $('#basic').show();

                },
                dateType: "json",
                data: {
                    'id': id
                },
                success: function (result) {
                    console.log(result)
                    if (result.code === 200) {
                        resultJson = result.data;
                        $('#basic').children('option:not(:first)').remove();
                        Object.keys(resultJson).forEach(function (key) {
                            var mySelect = document.getElementById('basic'),
                                newOption = document.createElement('option');
                            newOption.value = resultJson[key].user_id;
                            newOption.innerText = resultJson[key].u_nickname;

                            if(mySelect.length == 0){
                                mySelect.appendChild(newOption);
                            }else
                            {
                                mySelect.appendChild(newOption);
                            }
                        })
                        //con bug loi user khi change project, chua fix duoc
                        $('#basic').multiselect({
                            templates: {
                                li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
                            }
                        });
                    } else {
                        $(".multiselect-container").remove()
                    }
                }
            });
        }
    </script>
    <script src="{{asset('assets/js/project-pie.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}">
@endsection

