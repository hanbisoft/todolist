@extends('admin.master')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>
        {{ session()->get('message') }}</span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            @if(Auth::user()->is_Admin === 1)
                <a class="btn btn-info btn-register" href="{{ route('project.create') }}">New Project</a>
            @endif
        </div>
        <br>
        <br>
        <br>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">List Project</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-task">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>{{ trans('message.project_name') }}</th>
                                <th>{{ trans('message.date') }}</th>
                                <th>{{ trans('message.to') }}</th>
                                <th>{{ trans('message.project_type') }}</th>
                                <th>{{ trans('message.amount') }}</th>
                                <th>{{ trans('message.action') }}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function confirmDelete($id) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $(document).find('#frm_delete_' + $id).submit();
            } else {
                return false
            }
        }
        $(function () {
            $('#dataTables-task').DataTable({
                async: true,
                ajax: {
                    "url": "{{ route('api.project.showData') }}",
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                "order": [[ 0, "desc" ]],
                columns: [
                    {data: 'id',orderable: true},
                    {data: 'pName',searchable: true},
                    {data: 'psDate',orderable: false, searchable: false},
                    {data: 'peDate'},
                    {data: 'pType',orderable: true,searchable: true},
                    {data: 'amount',orderable: true,searchable: true},
                    {data: 'action', name: 'action', orderable: false, searchable: false,class:"text-center"},
                ]
            });
        });
    </script>
@endsection
