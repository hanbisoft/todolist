@extends('admin.master')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <h1>Add members to the project</h1>
            <div class="card">
                <form action="{{ route('project.createMember') }}" method="POST">
                    @csrf
                    <input type="hidden" name="project_id" value="{{ $id }}">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <h4>All members of your company</h4>
                                <br>
                                <ul class="list-group">
                                    <li class="list-group-item"><input id="checkAll" class="form-check-input"
                                                                       type="checkbox" style="margin-left: 5px"> <span
                                                style="margin-left: 4em">All</span></li>
                                    @foreach ($user as $item)
                                        <li class="list-group-item">
                                            <input name="arrUser[]" class="checkItem form-check-input"  type="checkbox" value="{{ $item->id }}"  style="margin-left: 5px"> <span style="margin-left: 4em" class="text-center">{{ $item->name }} - {{$item->email}}</span></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h4>Current member</h4>
                                <br>
                                <ul class="list-group">
                                    @foreach ($check_member as $value)
                                        <li class="list-group-item">{{$value->username}} - {{$value->email}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h1></h1>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer ">
                        <div class="row">
                            <div class="col-md-9">
                                <button type="submit" class="btn  btn-info text-center">Add</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('#checkAll').click(function () {
            $(':checkbox.checkItem').prop('checked', this.checked);
        });
    </script>
@endsection
