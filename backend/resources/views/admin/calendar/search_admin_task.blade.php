@extends('admin.master')
@section('content')
    <link rel="stylesheet" href="{{asset('css/bootstrap-multiselect.css')}}">
    <link href='{{asset('../packages/core/main.css')}}' rel='stylesheet'/>
    <link href='{{asset('../packages/daygrid/main.css')}}' rel='stylesheet'/>
    <script src='{{asset('../packages/core/main.js')}}'></script>
    <script src='{{asset('../packages/interaction/main.js')}}'></script>
    <script src='{{asset('../packages/daygrid/main.js')}}'></script>
    <script src='{{asset('../assets/js/search-admin.js')}}'></script>
    <script src='{{asset('../assets/js/input-date.js')}}'></script>
    <div class="row">
        <div class="col-md-6">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link text-danger " href="{{route('admin-task.index')}}">Task List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{route('calendar-admin-task')}}">Calendar</a>
                </li>
            </ul>

        </div>
        <div class="col-md-6">
            <form action="{{route('search.CalendarAdmin')}}" method="GET" class="form-inline">
                <label for="">Project:</label>
                &ensp;
                <select name="p_code"  class="form-control" >

                    <option   value="{{ $pID }}"> {{$pName}}</option>
                    <option value=""> All </option>
                    @foreach($project as $value)
                        <option value="{{$value->id}}">{{$value->pName}}</option>
                    @endforeach
                </select>

                <label for="">Assign to:</label>
                &ensp;
                <select name="assign_to" id="" class="form-control margin_left" >
                    <option   value="{{ $uID }}"> {{$uName}}</option>
                    <option value=""> All </option>
                    @foreach($user as $value)
                        <option value="{{$value->id}}">{{$value->u_nickname}}</option>
                    @endforeach
                </select>
                &ensp;&ensp;
                <button type="submit" class="btn btn-info"><i class="fas fa-search"></i> Search</button>
            </form>
        </div>
    </div>
    <br>
    <br>
    <div id="wait"></div>
    <div id='calendar'></div>

    {{--    //modal--}}
    <div class="modal" tabindex="-1" role="dialog" id="createTask">

        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Task</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal" action="{{ route("project-task.store") }}" method="POST"
                      enctype="multipart/form-data" style="width: 100%">
                    @csrf
                    <input type="hidden" name="calendar" value="calendar">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <input type="hidden" name="c_code" value="{{ Auth::user()->c_code }}">
                            <input type="hidden" name="t_Done" value="0">
                            <div class="row">
                                <label class="col-md-3 col-form-label"><strong class="text-danger">*</strong> Task
                                    name</label>
                                <div class="col-md-6">
                                    <div class="form-group has-default bmd-form-group">
                                        <input type="text" class="form-control" name="tSubject"
                                               placeholder="Enter Task name"
                                               required>
                                    </div>
                                </div>
                                <input type="hidden" name="t_Done">

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Project</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="p_code" id="project_create" class="form-control" onchange="showUserProject()" required>
                                                <option value="0" readonly="">Please select project</option>
                                                @foreach($project as $value)
                                                    <option value="{{$value->id}}">{{$value->pName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    &ensp;&ensp;
                                    <label class="col-form-label">Assign to</label>
                                    <div class="bs-docs-example">
                                        <select class="custom-select" name="arrUser[]" id="basic" multiple="multiple" style="display: none;">
                                        </select>
                                    </div>
                                    <span id="waiting" ></span>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Date</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <input type="text" class="form-control" name="t_sDate" id="start_date"
                                                   placeholder="Select date" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="">

                                        <div class="form-group has-default bmd-form-group">
                                            <label class=" col-form-label"><strong class="text-danger">*</strong> To</label>
                                            <input type="text" class="form-control" name="t_eDate" id="end_date"
                                                   placeholder="Select to" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Level</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_diff" id="" class="form-control" required>
                                                <option selected value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value=5">5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Task Type</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Type" id="" class="form-control" required>
                                                <option value="2">Development</option>
                                                <option value="1">Bug</option>
                                                <option value="3">Unit Testing</option>
                                                <option value="4">Update</option>
                                                <option value="5">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-form-label">Previous Task</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Pred" id="" class="form-control">
                                                <option value="">None</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label"><strong class="text-danger">*</strong> Process</label>
                                    <div class="">
                                        <div class="form-group has-default bmd-form-group">
                                            <select name="t_Process" id="" class="form-control" required>
                                                <option value="1">New</option>
                                                <option value="2">In Progress</option>
                                                <option value="3">Resolved</option>
                                                <option value="4">Done</option>
                                                <option value="5">Closed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="col-form-label">Content</label>
                                    <textarea class="form-control" name="t_Contents" id="t_Contents" cols="30" rows="10"
                                              placeholder="Enter content..."></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <label class="col-md-4 col-form-label"><strong>&nbsp;</strong> Attachment</label>
                                <div class="col-md-8">
                                    <div>
                                        <input type="file"
                                               accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,application/pdf,application/vnd.ms-excel,image/png, image/jpeg"
                                               class="form-control" name="a_code" id="a_code"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--  //modal user  --}}

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{--    //show detail task--}}
    <div class="modal" tabindex="-1" role="dialog" id="Task_detail">
        <div class="modal-dialog modal-xl" id="Task_detail_closed" role="document">
            <div class="modal-content">
                <form action="{{route('update-task')}}" method="post">
                    @csrf
                    <input type="hidden" name="id" id="id" value="">
                    <div class="modal-header">
                        <div class="col-md-10">
                            <h5 class="modal-title" id="modal-title">Task -  </h5>
                        </div>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="wait2"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <nav>
                                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                           href="#nav-home" role="tab" aria-controls="nav-home"
                                           aria-selected="true"><strong class="text-justify">Task</strong></a>
                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                           href="#nav-profile" role="tab" aria-controls="nav-profile"
                                           aria-selected="false"><strong class="text-justify">SubTasks</strong></a>
                                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                           href="#nav-contact" role="tab" aria-controls="nav-contact"
                                           aria-selected="false"><strong class="text-justify">Task Logs</strong></a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <table class="table" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Process</th>
                                                <th>Task type</th>
                                                <th>% Done</th>
                                                <th>Project name</th>
                                                <th>Assign</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><span id="title"></span></td>
                                                <td id="process"></td>
                                                <td id="t_task_type"></td>
                                                <td id="tabel_done"></td>
                                                <td id="project_name"></td>
                                                <td id="assign_name"></td>
                                                <td id="url"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        <br>
                                        <h3># Update Task</h3>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Assign to</label>
                                                    <select name="assign_to" id="assign_user" class="form-control">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for=""> % Done</label>
                                                    <select name="t_Done" id="t_Done" class="form-control">

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Process</label>
                                                    <select name="t_Process" id="t_Process" class="form-control"
                                                            required>

                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for=""><strong class="text-danger">*</strong> Note</label>
                                                    <textarea name="task_commit" id="" class="form-control" cols="30"
                                                              rows="10" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            @if(Auth::user()->is_Admin === 1)
                                                <div class="col-md-6">
                                                    <a  id="delete" style="margin-left: 1em"></a> <a  id="create_sub_task" style="float: left;text-align: right;"></a>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            @else
                                                <div class="col-md-12 text-right">
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                         aria-labelledby="nav-profile-tab">

                                        <table class="table" cellspacing="0">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Title</th>
                                                <th>Date</th>
                                                <th>To</th>
                                                <th>Assign</th>
                                                <th>% Done</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody id="sub_task">

                                            </tbody>
                                        </table>
                                        <br>
                                        <hr>

                                    </div>
                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                         aria-labelledby="nav-contact-tab">
                                        <ul class="list-group" id="logs">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">


                    </div>
                </form>
            </div>
        </div>

    </div>

    <script src="{{asset('js/bootstrap-multiselect.js')}}"></script>
    <script>
        CKEDITOR.replace('t_Contents',{
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        var expanded = false;
    </script>
    <script>

    </script>

@endsection