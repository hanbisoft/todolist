@extends('admin.master')
@section('title')
    Dashboard
@endsection
@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
    <script src='{{asset('../assets/js/input-date.js')}}'></script>
    <script src='{{asset('../assets/js/dashboard-mytask.js')}}'></script>
    <div class="animated fadeIn">
        <div class="row">
{{--            <div class="col-md-12">--}}
{{--                <h2>{{ trans('message.developer_screen') }}</h2>--}}
{{--                <br>--}}
{{--            </div>--}}
            <div class="col-sm-6 col-lg-3">
                <a style="color:#fff" href="{{route('project.index')}}">
                <div class="card text-white bg-primary">
                    <div class="card-body pb-0">
                        @foreach($project as $p)
                        <div class="text-value">{{$p->total}}</div>
                        <div>Project</div>
                        @endforeach
                    </div>
                    <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                        <canvas class="chart" id="card-chart1" height="70"></canvas>
                    </div>
                </div>
                </a>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-info">
                    <a style="color:#fff" href="{{route('my-task')}}">
                    <div class="card-body pb-0">
                        <button class="btn btn-transparent p-0 float-right" type="button">
                            <i class="icon-location-pin"></i>
                        </button>
                        <div class="text-value">
                            @foreach($countTask as $value)
                            {{$value->total}}</div>
                        @endforeach
                        <div>Task</div>
                    </div>
                    <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                        <canvas class="chart" id="card-chart2" height="70"></canvas>
                    </div>
                    </a>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-warning">
                    <div class="card-body pb-0">
                        <div class="btn-group float-right">
                            <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                        <div class="text-value">9.823</div>
                        <div>Members online</div>
                    </div>
                    <div class="chart-wrapper mt-3" style="height:70px;">
                        <canvas class="chart" id="card-chart3" height="70"></canvas>
                    </div>
                </div>
            </div>
            <!-- /.col-->
            <div class="col-sm-6 col-lg-3">
                <div class="card text-white bg-danger">
                    <div class="card-body pb-0">
                        <div class="btn-group float-right">
                            <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                        <div class="text-value">9.823</div>
                        <div>Members online</div>
                    </div>
                    <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                        <canvas class="chart" id="card-chart4" height="70"></canvas>
                    </div>
                </div>
            </div>
            <!-- /.col-->
        </div>
        <!-- /.row-->
        <div class="row bg-top">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Today</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-task">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Assign name</th>
                                    <th>Title</th>
                                    <th>Progress</th>
                                    <th>Due date</th>
                                    <th>Last modified date</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-->

        <!-- /.row-->

        <!-- /.row-->
    </div>
@endsection
