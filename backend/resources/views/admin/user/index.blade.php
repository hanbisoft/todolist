@extends('admin.master')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets/js/user.js')}}"></script>
    @if(session()->has('message'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
            </button>
            <span>
        {{ session()->get('message') }}</span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                   <div class="row">
                       <div class="col-md-8">
                           <h4 class="card-title ">User List</h4>
                       </div>
                       <div class="col-md-4 text-right">
                           @if(Auth::user()->is_Admin === 1)
                           <button onclick="delete_confirm()" class="btn btn-danger btn-register"><i class="fas fa-trash-alt"></i>  Delete All</button>
                           @endif
                       </div>
                   </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <form action="{{route('user-delete-all')}}" method="Post" id="deleteAll" >
                            @csrf
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-task">
                                <thead>
                                <tr>
                                    <th class="text-center" style="padding-left: 34px"><input type="checkbox" id="select_all" value=""/></th>
                                    <th>ID</th>
                                    <th>Name </th>
                                    <th>Nick name </th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function confirmDelete($id) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $(document).find('#frm_delete_' + $id).submit();
            } else {
                return false
            }
        }

        function delete_confirm(){
            if($('.checkbox:checked').length > 0){
                var result = confirm("Are you sure to delete selected users?");
                if(result){
                    $(document).find('#deleteAll').submit();
                }else{
                    return false;
                }
            }else{
                alert('Select at least 1 record to delete.');
                return false;
            }
        }
        $(document).ready(function(){
            $('#select_all').on('click',function(){
                if(this.checked){
                    $('.checkbox').each(function(){
                        this.checked = true;
                    });
                }else{
                    $('.checkbox').each(function(){
                        this.checked = false;
                    });
                }
            });

            $('.checkbox').on('click',function(){
                if($('.checkbox:checked').length == $('.checkbox').length){
                    $('#select_all').prop('checked',true);
                }else{
                    $('#select_all').prop('checked',false);
                }
            });
        });
        
        function upgradeManager() {
            if (confirm("Are you sure to upgrade this person to manager?") == true) {
               alert("OK")
            } else {
                return false
            }
        }
    </script>
@endsection
