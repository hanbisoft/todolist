<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\SubTaskM_Item;
use Illuminate\Support\Facades\DB;
class SubTaskItemEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return SubTaskM_Item::class;
    }

    public function selecTaskItem($getId)
    {
        $t = "SELECT * FROM sub_task_m__items s WHERE s.t_code = $getId";
        $SubTaskItem= DB::select($t);
        return $SubTaskItem;
    }


}
