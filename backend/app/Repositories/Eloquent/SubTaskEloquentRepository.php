<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\SubTaskM;
use Illuminate\Support\Facades\DB;
class SubTaskEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return SubTaskM::class;
    }


}
