<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\Project_item;
use Illuminate\Support\Facades\DB;
class ProjectItemEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return Project_item::class;
    }

    static public function findProjectItem($project)
    {
        $s = "SELECT p.id as pr_id,p.u_id, p.p_code, u.name as username, u.u_nickname as u_nickname, u.email as email, u.id as user_id FROM project_items p LEFT JOIN users u ON p.u_id = u.id
        WHERE p.p_code = $project";
        $projectItem = DB::select($s);
        return $projectItem;
    }


    public function showMember($id)
    {
        $p = "SELECT
        u.`name`,
        u.email,
        pi.id,
        u.id AS user_id
        FROM
            project_items pi
        LEFT JOIN users u ON u.id = pi.u_id
        WHERE
            pi.p_code = $id";
        $project_item =  DB::select($p);
        return $project_item;

    }
}
