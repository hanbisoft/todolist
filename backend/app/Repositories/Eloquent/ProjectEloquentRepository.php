<?php

namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\Project;
use Illuminate\Support\Facades\DB;

class ProjectEloquentRepository extends BaseEloquentRepository
{

    /**
     * @return mixed
     */
    public function model()
    {
        return Project::class;
    }

    static function showTask($id, $user)
    {
        $project =  DB::table('task_m_s')
            ->select('task_m_s.*','users.u_nickname as name','users.u_nickname as u_nickname')
            ->leftJoin('users', 'users.id', '=', 'task_m_s.assign_to')
            ->where('task_m_s.p_code',$id)
            ->orderByRaw('created_at  DESC')
            ->paginate(40);
        return $project;
    }

    /**
     * @return array
     */
    static public function count()
    {
        $p = "SELECT COUNT(id) as total FROM projects";
        $project = DB::select($p);
        return $project;
    }

    static  public function showData()
    {
        $p = "SELECT p.*, FORMAT(p.pAmount, 2) AS amount FROM projects p Order by p.created_at DESC";
        $project = DB::select($p);
        return $project;
    }

    static  public function showProjectOnlyUser($userId)
    {
        $p = "SELECT p.*, FORMAT(p.pAmount, 2) AS amount, pi.u_id as u_id FROM projects p LEFT JOIN project_items pi ON pi.p_code = p.id where pi.u_id = $userId ";
        $project = DB::select($p);
        return $project;
    }

    static public function checkUserAddMember($id)
    {
        $p = "SELECT
                p.*, u.id as user_id, u.name as username, u.email as email
            FROM
                project_items p
            LEFT JOIN users u ON u.id = p.u_id
            WHERE
                p.p_code = $id
            ";
        $project = DB::select($p);
        return $project;
    }

    /**
     * @param $id
     * @return array
     */
    static public function countTaskProject($id)
    {
        $p = "SELECT COUNT(t.id) AS total FROM projects p LEFT JOIN task_m_s t ON t.p_code = p.id WHERE p.id = $id AND t.p_code = $id ";
        $project = DB::select($p);
        return $project;
    }

    /**
     * @param $id
     * @return array
     */
    static public function countTaskProjectDone($id)
    {
        $p = "SELECT COUNT(t.id) AS total FROM projects p LEFT JOIN task_m_s t ON t.p_code = p.id WHERE p.id = $id AND t.p_code = $id AND t.t_Done = 100 AND t.t_Process = 4";
        $project = DB::select($p);
        return $project;
    }

    static public function CountTaskProjectDelay($id)
    {
        $dateNow = date("Y-m-d");
        $p = "SELECT COUNT(t.id) AS total FROM projects p LEFT JOIN task_m_s t ON t.p_code = p.id WHERE p.id = $id AND t.p_code = $id AND t.t_Done < 100 AND t.t_Process != 4 AND t.t_sDate < '$dateNow'";
        $project = DB::select($p);
        return $project;
    }
}
