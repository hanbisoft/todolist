<?php

namespace App\Repositories\Eloquent;

use App\Repositories\BaseEloquentRepository;
use App\Models\TaskM;
use Illuminate\Support\Facades\DB;

class ReportEloquentRepository extends BaseEloquentRepository
{

    /**
     * @return mixed
     */
    public function model()
    {
        return TaskM::class;
    }
    
    static public  function apiShowCountTask($id, $start_date,$end_date)
    {
        $r = "SELECT 
		    (
		    SELECT count(id) FROM task_m_s task WHERE task.t_Done = 100 AND task.p_code = $id
			) * 100 AS cnt_task_done,
			(
				SELECT count(id) FROM task_m_s task WHERE task.p_code = $id
			) * 100 AS cnt_total_task,
			(
				SELECT count(id) FROM task_m_s task WHERE task.p_code = $id AND task.t_Process != 4 AND task.t_Done < 100
			) AS cnt_task_delay
			
		FROM projects p
        LEFT JOIN task_m_s t ON t.p_code = p.id
        WHERE t.p_code = $id And p.psDate  BETWEEN  '$start_date' AND '$end_date'
        GROUP BY t.p_code";
        // echo $r;die;

        $report = DB::select($r);
        return $report;
    }
  
}
