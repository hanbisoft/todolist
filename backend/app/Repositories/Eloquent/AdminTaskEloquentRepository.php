<?php

namespace App\Repositories\Eloquent;

use App\Repositories\BaseEloquentRepository;
use App\Models\TaskM;
use Illuminate\Support\Facades\DB;

class AdminTaskEloquentRepository extends BaseEloquentRepository
{

    /**
     * @return mixed
     */
    public function model()
    {
        return TaskM::class;
    }

    /**
     * @return array
     */
    static function showAllTask()
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
            t.*, u.u_nickname AS assign_name,
            p.pName,
            u.u_nickname as u_nickname,
            CASE
                 WHEN t.t_diff = 1 THEN 'Very difficult'
                WHEN t.t_diff = 2 THEN 'Difficult'
                WHEN t.t_diff = 3 THEN 'Normal'
                WHEN t.t_diff = 4 THEN 'Easy'
                WHEN t.t_diff = 5 THEN 'Basic'
            END AS level,
            CASE
                WHEN t.t_Process = 1 THEN 'New'
                WHEN t.t_Process = 2 THEN 'In Progress'
                WHEN t.t_Process = 3 THEN 'Resolved'
                    WHEN t.t_Process = 4 THEN 'Done'
                    WHEN t.t_Process = 5 THEN 'Closed'
            END AS process,
            CASE
                WHEN t.t_Type = 1 THEN 'Bug'
                WHEN t.t_Type = 2 THEN 'Development'
                WHEN t.t_Type = 3 THEN 'Unit Testing'
                    WHEN t.t_Type = 4 THEN 'Update'
                    WHEN t.t_Type = 5 THEN 'Other'
            END AS task_type
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN projects p ON t.p_code = p.id
        ORDER BY t.created_at DESC ";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }

    static public function showTaskDueDate()
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
                t.*, u.u_nickname AS assign_name,
                a.a_file,
                a.a_fileName,
                p.pName,
                u.u_nickname AS u_nickname,
                CASE
            WHEN t.t_diff = 1 THEN 'Very difficult'
            WHEN t.t_diff = 2 THEN 'Difficult'
            WHEN t.t_diff = 3 THEN 'Normal'
            WHEN t.t_diff = 4 THEN 'Easy'
            WHEN t.t_diff = 5 THEN 'Basic'
            END AS LEVEL,
             CASE
            WHEN t.t_Process = 1 THEN
                'New'
            WHEN t.t_Process = 2 THEN
                'In Progress'
            WHEN t.t_Process = 3 THEN
                'Resolved'
            WHEN t.t_Process = 4 THEN
                'Done'
            WHEN t.t_Process = 5 THEN
                'Closed'
            END AS process,
             CASE
            WHEN t.t_Type = 1 THEN
                'Bug'
            WHEN t.t_Type = 2 THEN
                'Development'
            WHEN t.t_Type = 3 THEN
                'Unit Testing'
            WHEN t.t_Type = 4 THEN
                'Update'
            WHEN t.t_Type = 5 THEN
                'Other'
            END AS task_type
            FROM
                task_m_s t
            LEFT JOIN users u ON u.id = t.assign_to
            LEFT JOIN attachments a ON a.t_code = t.id
            LEFT JOIN projects p ON t.p_code = p.id
            WHERE
                t.t_eDate >= '$dateNow'
            AND t.t_Process <> 4 AND t.t_Done <  100
            ORDER BY
                t.created_at DESC";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }
    /**
     * @return array
     */
    static public function showCalendarAdmin()
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
            t.*, 
            u. NAME AS assign_name,
            u. u_nickname AS u_nickname,
            p.pName,
            u.u_color,
            CASE
                WHEN t.t_diff = 1 THEN 'Very difficult'
                WHEN t.t_diff = 2 THEN 'Difficult'
                WHEN t.t_diff = 3 THEN 'Normal'
                WHEN t.t_diff = 4 THEN 'Easy'
                WHEN t.t_diff = 5 THEN 'Basic'
            END AS level,
            CASE
                WHEN t.t_Process = 1 THEN 'New'
                WHEN t.t_Process = 2 THEN 'In Progress'
                WHEN t.t_Process = 3 THEN 'Resolved'
                    WHEN t.t_Process = 4 THEN 'Done'
                    WHEN t.t_Process = 5 THEN 'Closed'
            END AS process,
            CASE
                WHEN t.t_Type = 1 THEN 'Bug'
                WHEN t.t_Type = 2 THEN 'Development'
                WHEN t.t_Type = 3 THEN 'Unit Testing'
                    WHEN t.t_Type = 4 THEN 'Update'
                    WHEN t.t_Type = 5 THEN 'Other'
            END AS task_type
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN projects p ON t.p_code = p.id";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }

    static public function showSubTaskByID($id)
    {
        $t = "SELECT
            t.id as TaskID,
            t.tSubject as TaskName,
            t.t_sDate as StartDate,
            t.t_eDate as EndDate,
            t.t_Done as Progress,
            u.u_nickname as u_nickname
            FROM
                task_m_s t
            LEFT JOIN users u ON u.id = t.assign_to
            WHERE
            t.parent_id = $id";
        $task = DB::select($t);
        return $task;
    }


    static public function searchGantt($assign_to, $p_code)
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
            t.*, 
            u.u_nickname AS u_nickname,
            a.a_file,
            a.a_fileName,
            p.pName,
           u.u_color,
            CASE
                WHEN t.t_diff = 1 THEN 'Very difficult'
                WHEN t.t_diff = 2 THEN 'Difficult'
                WHEN t.t_diff = 3 THEN'Normal'
                WHEN t.t_diff = 4 THEN'Easy'
                WHEN t.t_diff = 5 THEN'Basic'
            END AS level,
            CASE
                WHEN t.t_Process = 1 THEN 'New'
                WHEN t.t_Process = 2 THEN 'In Progress'
                WHEN t.t_Process = 3 THEN 'Resolved'
                    WHEN t.t_Process = 4 THEN 'Done'
                    WHEN t.t_Process = 5 THEN 'Closed'
            END AS process,
            CASE
                WHEN t.t_Type = 1 THEN 'Bug'
                WHEN t.t_Type = 2 THEN 'Development'
                WHEN t.t_Type = 3 THEN 'Unit Testing'
                    WHEN t.t_Type = 4 THEN 'Update'
                    WHEN t.t_Type = 5 THEN 'Other'
            END AS task_type
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN attachments a ON a.t_code = t.id
        LEFT JOIN projects p ON t.p_code = p.id
        WHERE ( $assign_to =  '' or t.assign_to = $assign_to) AND ($p_code ='' or t.p_code = $p_code)";
        $task = DB::select($t);
        return $task;
    }


    static public function pieChart()
    {
        $t = "SELECT
        DISTINCT
            p.id as project_id,
            p.pName,
            p.pType,
         (select count(p_code) from task_m_s where p_code = t.p_code and t_Done = 100) * 100 as cnt_task_done,
         (select count(id) from task_m_s where p_code = t.p_code) * 100 as cnt_total_task
        FROM
            projects p
        LEFT JOIN task_m_s t ON t.p_code = p.id Limit 4
        ";
        $task = DB::select($t);
        return $task;
    }

    static public function pieChartAll()
    {
        $t = "SELECT
        DISTINCT
            p.id as project_id,
            p.pName,
            p.pType,
         (select count(p_code) from task_m_s where p_code = t.p_code and t_Done = 100) * 100 as cnt_task_done,
         (select count(id) from task_m_s where p_code = t.p_code) * 100 as cnt_total_task
        FROM
            projects p
        LEFT JOIN task_m_s t ON t.p_code = p.id
        ";
        $task = DB::select($t);
        return $task;
    }

    static public function QuarterlyChart()
    {
        $t = "   SELECT 
               a.u_nickname,
                a.user_id
                , a.cnt_task_done
                , a.cnt_total_task
                , case when a.cnt_total_task = 0 then 0
                                else ROUND(a.cnt_task_done / a.cnt_total_task) * 100
                    end as percent
            FROM
            (
            SELECT
               
            DISTINCT
            u.id as user_id,
            u.u_nickname as u_nickname,
             (select count(p_code) from task_m_s task where task.t_Done = 100 and task.assign_to = u.id) as cnt_task_done,
             (select count(id) from task_m_s task where  task.assign_to = u.id)  as cnt_total_task	
						
            FROM
                projects p
            LEFT JOIN task_m_s t ON t.p_code = p.id
            LEFT JOIN users u On t.assign_to = u.id
            ) a
            ";
        $task = DB::select($t);
        return $task;
    }
}
