<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\User;
use Illuminate\Support\Facades\DB;
class UserEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }

    static public function ProjectItem($project_id)
    {
        $s = "SELECT p.u_id, u.name as username, u.email as email, u.id as user_id FROM project_items p LEFT JOIN users u ON p.u_id = u.id WHERE p.u_id = $project_id ";
        $User= DB::select($s);
        return $User;
    }

    static public function showMember($company_id)
    {
        $s = "SELECT u.id as id, u.name as name, p.u_id as u_id, u.email as email, p.id as pr_id  FROM users u LEFT JOIN project_items p ON p.u_id = u.id WHERE u.c_code =$company_id AND p.u_id is NULL";
        $User= DB::select($s);
        return $User;
    }

    static  public  function count()
    {
        $u = "Select Count(id) as total From users";
        $User= DB::select($u);
        return $User;
    }

    static public function showUserProject($id)
    {
        $u = "SELECT  u.name as username, u.u_nickname as u_nickname,p.u_id as user_id FROM project_items p LEFT JOIN users u On u.id = p.u_id WHERE p.p_code = $id";
        $User= DB::select($u);
        return $User;
    }
}
