<?php

namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\TaskM;
use Illuminate\Support\Facades\DB;

class TaskEloquentRepository extends BaseEloquentRepository
{

    /**
     * @return mixed
     */
    public function model()
    {
        return TaskM::class;
    }

    static public function delTask($id)
    {
        $s = "DELETE FROM task_m_s where task_m_s.id = $id OR task_m_s.parent_id = $id";
        $task = DB::select($s);
        return;
    }

    public function showTask($p_code, $assign_to)
    {
        $t = "SELECT * from task_m_s WHERE  parent_id is NULL and ( $p_code = '' or p_code = $p_code) and ( $assign_to = '' or assign_to = $assign_to) ";
        //    echo $t;die;
        $task = DB::select($t);
        return $task;

    }

    /**
     * @param $id
     * @return array
     */
    static public function showTaskID($id)
    {
        $t = "SELECT t.*, u. NAME AS username,u.u_nickname as u_nickname,u.id as user_id,a.a_file,t.assign_to as assign_id,  a.a_fileName,p.pName,p.id as p_id FROM task_m_s t
            LEFT JOIN users u ON u.id = t.assign_to
            LEFT JOIN attachments a ON a.t_code = t.id
            LEFT JOIN projects p ON t.p_code = p.id
             WHERE t.id = $id limit 1";
        $task = DB::select($t);
        return $task;
    }


    /**
     * @param $id
     * @return array
     */
    static public function subTask($id)
    {
        $t = "SELECT
        t.*, 
        u. NAME AS username,
        u.u_nickname as u_nickname,
        a.a_file,
        a.a_fileName,
        p.pName,
        p.id as p_id
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN attachments a ON a.t_code = t.id
        LEFT JOIN projects p ON t.p_code = p.id
        WHERE t.parent_id = $id";
        // echo $t;die;
        $task = DB::select($t);
        return $task;
    }

    /**
     * @param $id
     * @return array
     */
    static public function showMyTask($id)
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
            t.*, 
            u. NAME AS assign_name,
            u.u_nickname as u_nickname,
            a.a_file,
            a.a_fileName,
            t.updated_at,
            p.pName,
            CASE
                     WHEN t.t_diff = 1 THEN 'Very difficult'
                    WHEN t.t_diff = 2 THEN 'Difficult'
                    WHEN t.t_diff = 3 THEN'Normal'
                    WHEN t.t_diff = 4 THEN'Easy'
                    WHEN t.t_diff = 5 THEN'Basic'
            END AS level,
            CASE
                WHEN t.t_Process = 1 THEN 'New'
                WHEN t.t_Process = 2 THEN 'In Progress'
                WHEN t.t_Process = 3 THEN 'Resolved'
                    WHEN t.t_Process = 4 THEN 'Done'
                    WHEN t.t_Process = 5 THEN 'Closed'
            END AS process,
            CASE
                WHEN t.t_Type = 1 THEN 'Bug'
                WHEN t.t_Type = 2 THEN 'Development'
                WHEN t.t_Type = 3 THEN 'Unit Testing'
                    WHEN t.t_Type = 4 THEN 'Update'
                    WHEN t.t_Type = 5 THEN 'Other'
                    WHEN t.t_Type = 6 THEN 'Plan'
            END AS task_type
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN attachments a ON a.t_code = t.id
        LEFT JOIN projects p ON t.p_code = p.id
        WHERE t.assign_to = $id ";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }

    /**
     * @return array
     */
    static public function countTask($id)
    {
        $t = "SELECT  COUNT(id) as total FROM task_m_s t WHERE t.assign_to = $id";
        $task = DB::select($t);
        return $task;
    }

    static public function searchMyTask($assign_to, $p_code)
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
            t.*, 
            u.u_nickname AS assign_name,
            u.u_nickname AS u_nickname,
            a.a_file,
            a.a_fileName,
            p.pName,
           u.u_color,
            CASE
                WHEN t.t_diff = 1 THEN 'Very difficult'
                WHEN t.t_diff = 2 THEN 'Difficult'
                WHEN t.t_diff = 3 THEN'Normal'
                WHEN t.t_diff = 4 THEN'Easy'
                WHEN t.t_diff = 5 THEN'Basic'
            END AS level,
            CASE
                WHEN t.t_Process = 1 THEN 'New'
                WHEN t.t_Process = 2 THEN 'In Progress'
                WHEN t.t_Process = 3 THEN 'Resolved'
                    WHEN t.t_Process = 4 THEN 'Done'
                    WHEN t.t_Process = 5 THEN 'Closed'
            END AS process,
            CASE
                WHEN t.t_Type = 1 THEN 'Bug'
                WHEN t.t_Type = 2 THEN 'Development'
                WHEN t.t_Type = 3 THEN 'Unit Testing'
                    WHEN t.t_Type = 4 THEN 'Update'
                    WHEN t.t_Type = 5 THEN 'Other'
            END AS task_type
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN attachments a ON a.t_code = t.id
        LEFT JOIN projects p ON t.p_code = p.id
        WHERE ( $assign_to =  '' or t.assign_to = $assign_to) AND ($p_code ='' or t.p_code = $p_code)";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }

    static public function apiShowUserByIDProject($id)
    {
        $u = "SELECT u.id as user_id,u.u_nickname as u_nickname FROM project_items p LEFT JOIN users u ON u.id = p.u_id WHERE p.p_code = $id";
        $task = DB::select($u);
        return $task;
    }


    static public function showMyTaskToday($id)
    {
        $dateNow = date("Y-m-d");
        $t = "SELECT
            t.*, 
            u. NAME AS assign_name,
            u.u_nickname as u_nickname,
            a.a_file,
            a.a_fileName,
            t.updated_at,
            p.pName,
            CASE
                     WHEN t.t_diff = 1 THEN 'Very difficult'
                    WHEN t.t_diff = 2 THEN 'Difficult'
                    WHEN t.t_diff = 3 THEN'Normal'
                    WHEN t.t_diff = 4 THEN'Easy'
                    WHEN t.t_diff = 5 THEN'Basic'
            END AS level,
            CASE
                WHEN t.t_Process = 1 THEN 'New'
                WHEN t.t_Process = 2 THEN 'In Progress'
                WHEN t.t_Process = 3 THEN 'Resolved'
                    WHEN t.t_Process = 4 THEN 'Done'
                    WHEN t.t_Process = 5 THEN 'Closed'
            END AS process,
            CASE
                WHEN t.t_Type = 1 THEN 'Bug'
                WHEN t.t_Type = 2 THEN 'Development'
                WHEN t.t_Type = 3 THEN 'Unit Testing'
                    WHEN t.t_Type = 4 THEN 'Update'
                    WHEN t.t_Type = 5 THEN 'Other'
                    WHEN t.t_Type = 6 THEN 'Plan'
            END AS task_type
        FROM
            task_m_s t
        LEFT JOIN users u ON u.id = t.assign_to
        LEFT JOIN attachments a ON a.t_code = t.id
        LEFT JOIN projects p ON t.p_code = p.id
        WHERE t.assign_to = $id AND t.parent_id is null AND t.t_sDate = '$dateNow'";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }

    static public function showUserInTaskUpdate($id)
    {
        $t = "SELECT u.* FROM project_items pi
            LEFT JOIN users as u On u.id = pi.u_id
            where pi.p_code = $id";
//        echo $t;die;
        $task = DB::select($t);
        return $task;
    }
}
