<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\Company;

class CompanyEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return Company::class;
    }

}
