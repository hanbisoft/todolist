<?php
namespace App\Repositories\Eloquent;


use App\Repositories\BaseEloquentRepository;
use App\Models\Attachment;

class AttachmentEloquentRepository extends BaseEloquentRepository
{

     /**
     * @return mixed
     */
    public function model()
    {
        return Attachment::class;
    }

    public function getAttach(Type $var = null)
    {
        # code...
    }

}
