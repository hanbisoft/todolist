<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class SendMailForDues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('admin.mail.newtask', array(
            'email' => $this->data['email_member'],
            'name' => $this->data['name'],
            'task_name' => $this->data['task_name'],
            'assign_by' => $this->data['assign_by'],
            'task_id' => $this->data['task_id']

        ), function ($message)  {
            $message->to($this->data['email_member'], 'NEW TASK')->subject('NEW TASK')
            ->cc($this->data['email_member']);
        });
    }
}