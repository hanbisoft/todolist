<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\AttachmentEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use App\Repositories\Eloquent\TaskEloquentRepository;
use Auth;
use Illuminate\Pagination\Paginator;
use App\Models\TaskM;
use Input;
use Yajra\DataTables\DataTables;
use App\User;
use Session;
use App;
use Config;
class ProjectController extends Controller
{
    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $attachmentRepository;
    private $userRepository;
    private $taskRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        AttachmentEloquentRepository $attachmentRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository,
        TaskEloquentRepository $taskRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->attachmentRepository = $attachmentRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        return view('admin.project.index',compact('locale'));
    }

    public function showData()
    {
        $checkisAdmin = Auth::user()->is_Admin;
        $project = $this->projectRepository->showData();
        $userId = Auth::user()->id;
        $project_user = $this->projectRepository->showProjectOnlyUser($userId);
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $token = $encrypter->encrypt(csrf_token());
        if ($checkisAdmin === 1) {
            return Datatables::of($project)
                ->addColumn('action', function ($project) use ($token) {
                    return '
                <a class="btn btn-sm btn-info btn-round btn-register" style="margin-right:1em" href="/project/' . $project->id . '"> <i class="fas fa-eye"></i></a>
                <a class="btn  btn-sm btn-success btn-register" href="/project/' . $project->id . '/edit"> <i class="far fa-edit"></i></a>
                <a class="btn btn-sm btn-danger btn-register "style="margin-left:1em"  href="javascript:void(0);" onclick="return confirmDelete(' . $project->id . ')"> <i class="fas fa-trash-alt"></i></a>
                 <form action="project-delete" method="get" id="frm_delete_' . $project->id . '">
                    <input type="hidden" name="id" value='.$project->id.'>
                </form>';
                })
                ->make(true);
        } else {
            return Datatables::of($project_user)
                ->addColumn('action', function ($project) use ($token) {
                    return '
                <a class="btn btn-sm btn-info btn-round btn-register" href="/project/' . $project->id . '"> <i class="fas fa-eye"></i></a>
               ';
                })
                ->make(true);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_id = Auth::user()->c_code;
        $user = User::where('c_code',$company_id)->where('u_auth',1)->get();
        $company = $this->companyRepository->all();
        return view('admin.project.create', compact(['company','user']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //create project
        $project = $this->projectRepository->create($request->all());

        $user = Auth::user();

        //upload attachment file
        if ($request->hasFile('a_code')) {
            $excel = $request->file('a_code')->store('public/documents');
            $url_excel = $excel;
            $attachment = $this->attachmentRepository->create([
                'c_code' => $user->c_code,
                'p_code' => $project->id,
                'a_file' => $request->file('a_code')->getClientOriginalName(),
                'a_fileName' => $url_excel,
                'a_date' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
        }
        $arrUser = $request->get('arrUser');

        //insert
        foreach ($arrUser as $key => $item) {
            $addMember = $this->projectItemRepository->create([
                'u_id' => $item,
                'c_code' => $project->c_code,
                'p_name_item' => $project->pName,
                'pu_Auth' => 1,
                'p_code' => $project->id
            ]);
        }
//         return redirect()->back()->with('message', 'Create successful!');
        return redirect()->route('project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $project = $this->projectRepository->find($id);

        $project_item = $this->projectItemRepository->findProjectItem($project->id);
        $showTask = $this->projectRepository->showTask($id, $user);
//        return $showTask;
        $attachment = $this->attachmentRepository->findByField('p_code', $project->id);
        return view('admin.project.show', compact(['project', 'project_item', 'attachment', 'id', 'showTask']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = $this->companyRepository->all();
        $project = $this->projectRepository->find($id);

        return view('admin.project.edit', compact(['project', 'company']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        //upload attachment file
        if ($request->hasFile('a_code')) {
            $excel = $request->file('a_code')->store('public/documents');
            $url_excel = $excel;
            $attachment = $this->attachmentRepository->create([
                'c_code' => $user->c_code,
                'p_code' => $id,
                'a_file' => $url_excel,
                'a_fileName' => $request->file('a_code')->getClientOriginalName(),
                'a_date' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
        }

        $project = $this->projectRepository->update($request->all(), $id);
        return redirect()->route('project.index')->with('message', 'Update successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = $this->projectRepository->delete($id);
        return redirect()->back()->with('message', 'Delete successful!');
    }

    //** return view addmember */
    public function addMember($id)
    {
        $company_id = Auth::user()->c_code;
        $user = $this->userRepository->findByField('c_code', $company_id);
        $check_member = $this->projectRepository->checkUserAddMember($id);
        return view('admin.project.addmember', compact(['user', 'id','check_member']));
    }

    public function createMember(Request $request)
    {

        //todo get project
        $project = $this->projectRepository->find($request->get('project_id'));
        $arrUser = $request->get('arrUser');

        //insert
        foreach ($arrUser as $key => $item) {
            $addMember = $this->projectItemRepository->create([
                'u_id' => $item,
                'c_code' => $project->c_code,
                'p_name_item' => $project->pName,
                'pu_Auth' => 1,
                'p_code' => $project->id
            ]);
        }
        return redirect()->route('project.show', ['id' => $project->id]);
    }


    public function UpdateProcess(Request $request)
    {
        //    return $request->get('t_Process');
        $task = TaskM::where('id', $request->get('get_id_task'))
            ->update(['t_Process' => $request->get('t_Process')]);
        return redirect()->back()->with('message', 'Update successfully!');
    }

    /**
     * @param Request $request
     */
    public function destroyProject(Request $request)
    {
        $id = $request->get('id');
        $project = $this->projectRepository->delete($id);
        $project_item = $this->projectItemRepository->findByField('p_code',$id);
        foreach ($project_item as $key => $item) {
            $this->projectItemRepository->delete($item->id);
        }
        return redirect()->back()->with('message', 'Deleted successfully');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiCountTaskProject(Request $request)
    {
        $id = $request->get('id');
        $project = $this->projectRepository->find($id);
        if($id)
        {
            $totals = $this->projectRepository->countTaskProject($id);
            $done  = $this->projectRepository->countTaskProjectDone($id);
            $delay  = $this->projectRepository->CountTaskProjectDelay($id);
            return response()->json([
                'total' => $totals,
                'done' => $done,
                'delay' => $delay,
                'pName' =>$project->pName,
            ]);
        }
        return response()->json([
            'total' => null,
            'done' => null,
            'delay' => null
        ]);


    }
}
