<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
class DownloadController extends Controller
{
     public function download(Request $request)
    {
        return Storage::download($request->get('url'));

    }
}
