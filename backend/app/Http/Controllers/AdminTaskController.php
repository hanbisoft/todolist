<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use App\Repositories\Eloquent\AdminTaskEloquentRepository;
use Auth;
use Session;
use App;
use Config;
use Yajra\DataTables\DataTables;
use App\Models\TaskLog;

class AdminTaskController extends Controller
{
    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $userRepository;
    private $admintaskRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository,
        AdminTaskEloquentRepository $admintaskRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
        $this->admintaskRepository = $admintaskRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $task = $this->admintaskRepository->findByField('parent_id', null);
        $user = $this->userRepository->all();
        $project = $this->projectRepository->all();
        return view('admin.task.index', compact(['task', 'project', 'user']));
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function showAllTask()
    {
        $task = $this->admintaskRepository->showAllTask();
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $token = $encrypter->encrypt(csrf_token());
        // return $token;
        return Datatables::of($task)
            ->addColumn('action', function ($task) use ($token) {
                return '
                 <a class="btn btn-sm btn-info btn-round" href="/project-task/' . $task->id . '"> <i class="fas fa-eye"></i></a>
                ';
            })
            ->make(true);
    }

    public function showTaskDueDate()
    {
        $taskDueDate = $this->admintaskRepository->showTaskDueDate();
        return Datatables::of($taskDueDate)->make(true);
    }
    //only admin show

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendarTaskAdmin()
    {
        $user = Auth::user();
        $project = $this->projectRepository->all();
//        return $project;
        $user = $this->userRepository->all();
        return view('admin.calendar.all-task-calendar', compact(['project', 'user']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchCalendarAdmin(Request $request)
    {
//        return $request->all();
        $user = Auth::user();
        if ($request->get('p_code')) {
            $project_by_id = $this->projectRepository->find($request->get('p_code'));
            $pName = $project_by_id->pName;
            $pID = $project_by_id->id;
        } else {
            $pName = "All";
            $pID = "";
        }
        if ($request->get('assign_to')) {
            $user_by_id = $this->userRepository->find($request->get("assign_to"));
            $uName = $user_by_id->u_nickname;
            $uID = $user_by_id->id;
        } else {
            $uName = "All";
            $uID = "";
        }

        $project = $this->projectRepository->all();
        $user = $this->userRepository->findByField('u_auth',1);
        return view('admin.calendar.search_admin_task', compact(['project', 'user', 'pName', 'pID', 'uID', 'uName']));

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiShowAdminCalendar()
    {
        $user = Auth::user();
        $id = $user->id;
        $task = $this->admintaskRepository->showCalendarAdmin();
        $arrData = [];
        foreach ($task as $value) {
            $color = Config::get('color.DEV');
            if ($value->task_type == "Development") {
                $color = Config::get('color.DEV');
            } elseif ($value->task_type == "Unit Testing") {
                $color = Config::get('color.TEST');
            } elseif ($value->task_type == "Update") {
                $color = Config::get('color.UPDATE');
            } elseif ($value->task_type == "Other") {
                $color = Config::get('color.ETC');
            } elseif ($value->task_type == "Bug") {
                $color = Config::get('color.DEBUG');
            }
            $end_date = $value->t_eDate;
            $end_date1 = str_replace('-', '/', $end_date);
            $tomorrow = date('Y-m-d', strtotime($end_date1 . "+1 days"));
            $data = [
                'id' => $value->id,
                'start' => $value->t_sDate,
                'end' => $tomorrow,
                'title' => $value->tSubject . ' - '.  $value->t_Done. '%' . ' - ' . $value->u_nickname,
                'backgroundColor' => $color,
                'eventColor' => '#ffffff',
            ];
            array_push($arrData, $data);
        }
        return response()->json($arrData);
    }


    public function apiUpdateTask(Request $request)
    {
        if ($request->get('id')) {
            $task = $this->admintaskRepository->update($request->all(), $request->get('id'));
            //log
            $task_commit = TaskLog::create([
                't_code' => $request->get('id'),
                'l_status' => "Updated by: " . '<span class="text-info">' . Auth::user()->u_nickname . '</span>' . " about: " . '<span class="text-info">' . date('Y-m-d H:i:s') . '</span>' . " ago ",
                'l_content' => "Change date by manager",
                'l_user_id' => $request->get('id'),
                'time_log' => date('Y-m-d H:i:s')
            ]);
            return response()->json([
                'code' => 200,
                'mess' => "Update success fully"
            ]);

        } else {
            return response()->json([
                'code' => 400,
                'mess' => null
            ]);
        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTask(Request $request)
    {
//       return $request->all();
        //update task
        $task = $this->admintaskRepository->update($request->all(), $request->id);
        $user = Auth::user()->u_nickname;
        $task_commit = TaskLog::create([
            't_code' => $request->get('id'),
            'l_status' => "Updated by: " . '<span class="text-info">' . $user . '</span>' . " about: " . '<span class="text-info">' . date('Y-m-d H:i:s') . '</span>' . " ago ",
            'l_content' => $request->get('task_commit'),
            'l_user_id' => $request->get('assign_to'),
            'time_log' => date('Y-m-d H:i:s')
        ]);
        return redirect()->back();
    }


    public function deleteSubTask($id)
    {
        $this->admintaskRepository->delete($id);
        return redirect()->back()->with('message', 'Delete successful!');
    }

}
