<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\Eloquent\AdminTaskEloquentRepository;
class AdminChartController extends Controller
{
    private $admintaskRepository;

    function __construct(
        AdminTaskEloquentRepository $admintaskRepository
    )
    {
        $this->admintaskRepository = $admintaskRepository;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiPieChart(Request $request)
   {

       $page = $request->get('page');
       if($page)
       {
           $task = $this->admintaskRepository->pieChartAll();
           $chart2 = $this->admintaskRepository->QuarterlyChart();
           return response()->json([
               'chart1' => $task,
               'chart2' => $chart2
           ]);
       }else {
           $task = $this->admintaskRepository->pieChart();
           $chart2 = $this->admintaskRepository->QuarterlyChart();
           return response()->json([
               'chart1' => $task,
               'chart2' => $chart2
           ]);
       }

   }


}
