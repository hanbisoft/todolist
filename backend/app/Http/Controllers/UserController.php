<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\UserEloquentRepository;
use Auth;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\AttachmentEloquentRepository;
use App\Repositories\Eloquent\TaskEloquentRepository;
use App\Models\Attachment;
use App\Models\TaskLog;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\Datatables\Datatables;
use Session;
use App;
use Config;

class UserController extends Controller
{

    private $useRepository;

    function __construct(
        UserEloquentRepository $useRepository
    )
    {
        $this->useRepository = $useRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $user = $this->useRepository->find($id);
        // return $user->name;

        return view('admin.pages.user', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            //rename
            $filename = str_random(4) . $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        }else{
            $urlFile = Auth::user()->avatar;
        }
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'u_phone' => $request->get('u_phone'),
            'u_color' => $request->get('u_color'),
            'u_cost' => $request->get('u_cost'),
            'avatar' => $urlFile
        ];
        //    return $request->all();
        $user = $this->useRepository->update($data, $id);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUser(Request $request)
    {
        $id = $request->get('id');
        $user = $this->useRepository->showUserProject($id);
        if($user)
        {
            return response()->json([
                'code'=> 200,
                'data' => $user
            ]);
        }else {
            return response()->json([
                'code'=> 400,
                'data' => []
            ]);
        }

    }
}
