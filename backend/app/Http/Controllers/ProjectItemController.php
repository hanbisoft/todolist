<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\AttachmentEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
class ProjectItemController extends Controller
{

    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $attachmentRepository;
    private $userRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        AttachmentEloquentRepository $attachmentRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->attachmentRepository = $attachmentRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project_item = $this->projectItemRepository->delete($id);
        return redirect()->back();
    }
}
