<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use App\Repositories\Eloquent\AdminTaskEloquentRepository;
use Auth;
use Session;
use App;
use Config;
use Yajra\DataTables\DataTables;
use App\Models\TaskLog;
use Carbon\Carbon;

class GrantChartController extends Controller
{
    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $userRepository;
    private $admintaskRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository,
        AdminTaskEloquentRepository $admintaskRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
        $this->admintaskRepository = $admintaskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = $this->projectRepository->all();
        $user = $this->userRepository->findByField('u_auth',1);
        return view('admin.grantt.index',compact(['user','project']));
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiGrantChart()
    {
        $task = $this->admintaskRepository->showAllTask();
//        return $task;
        $arrData = [];
        $arrDataID = [];


        $sub_task = $this->admintaskRepository->findWhereIn('parent_id', $arrDataID);

        foreach ($task as $value) {
            $color =  "";

            $start_date = date('m/d/Y', strtotime($value->t_sDate));

            $end_date = date('m/d/Y', strtotime($value->t_eDate));
//            $this->admintaskRepository->showSubTaskByID($value->id)
            $data = [
                'TaskID' => $value->id,
                'TaskName' => $value->tSubject  . '-' . '<b>'.$value->u_nickname.'</b>' ,
                'StartDate' => $start_date,
                'EndDate' => $end_date,
                'Progress' => $value->t_Done,
                "isManual" => true,
                "SubTasks" => $this->admintaskRepository->showSubTaskByID($value->id),
                "resourceId"=> [3],
                "Color" =>  $color,
                "resourceName" => $value->u_nickname
            ];
            array_push($arrData, $data);
        }
        //return json
        return response()->json($arrData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiSearchGanttChart(Request $request)
    {
        $assign_to = $request->get('assign_to');
        $p_code = $request->get('p_code');

        $task = $this->admintaskRepository->searchGantt($assign_to,$p_code);
        $arrData = [];
        $arrDataID = [];

        foreach ($task as $item) {
            $dataID = $item->id;
            array_push($arrDataID, $dataID);
        }
        $sub_task = $this->admintaskRepository->findWhereIn('parent_id', $arrDataID);

        foreach ($task as $value) {
            $color =  "";
            $start_date = date('m/d/Y', strtotime($value->t_sDate));
            $end_date = date('m/d/Y', strtotime($value->t_eDate));

            $data = [
                'TaskID' => $value->id,
                'TaskName' => $value->tSubject  . '-' . '<b>'.$value->u_nickname.'</b>' ,
                'StartDate' => $start_date,
                'EndDate' => $end_date,
                'Progress' => $value->t_Done,
                "isManual" => true,
                "SubTasks" => $this->admintaskRepository->showSubTaskByID($value->id),
                "resourceId"=> [3],
                "Color" =>  $color,
                "resourceName" => $value->u_nickname
            ];
            array_push($arrData, $data);
        }
        //return json
        return response()->json($arrData);
    }
    /**
     * @param Request $request
     */
    public function searchGantt (Request $request)
    {
        if ($request->get('p_code')) {
            $project_by_id = $this->projectRepository->find($request->get('p_code'));
            $pName = $project_by_id->pName;
            $pID = $project_by_id->id;
        } else {
            $pName = "All";
            $pID = "";
        }
        if ($request->get('assign_to')) {
            $user_by_id = $this->userRepository->find($request->get("assign_to"));
            $uName = $user_by_id->u_nickname;
            $uID = $user_by_id->id;
        } else {
            $uName = "All";
            $uID = "";
        }
        $project = $this->projectRepository->all();
        $user = $this->userRepository->findByField('u_auth',1);
        return view('admin.grantt.search-gantt',compact(['project','user','uID','uName','pName','pID']));
    }




}
