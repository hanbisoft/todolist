<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\User;
use App\Repositories\Eloquent\UserEloquentRepository;
class AdminUserController extends Controller
{
    private $userRepository;

    function __construct(
        UserEloquentRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    public function updateUser($id)
    {
        $user = User::find($id);
        $user->u_auth =  1;
        $user->save();
        return redirect()->back()->with('message','Update successfully');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function showData()
    {
        $dataUser = [];
        $u = "select u.*,c.c_name from users u LEFT JOIN companies c ON c.id = u.c_code ORDER BY u.created_at DESC";
        $user = DB::select($u);;
        $encrypter = app('Illuminate\Encryption\Encrypter');
        return Datatables::of($user)
            ->addColumn('action', function ($user){
                return '
             <a style="margin-right:10px" class="btn  btn-sm btn-info btn-register" href="/upgrade-user/' . $user->id . '" data-toggle="tooltip" title="Update  manager to member "> <i class="fas fa-arrow-circle-up"></i>  </a>
             <a class="btn  btn-sm btn-success btn-register" href="/upgrade-manager/' . $user->id . '" data-toggle="tooltip" title="Update member to manager "> <i class="fas fa-arrow-circle-down"></i> </a>
            <a style="margin-left:10px" class="btn btn-sm btn-danger  btn-register" data-toggle="tooltip" title="Delete user " href="javascript:void(0);" onclick="return confirmDelete(' . $user->id . ')"> <i class="fas fa-trash-alt"></i> </a>
                 <form action="user-delete" method="get" id="frm_delete_' . $user->id . '">
                    <input type="hidden" name="id" value='.$user->id.'>
                </form>
           ';
            })
            ->make(true);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userDelete(Request $request)
    {
        $id = $request->id;
        if($id)
        {
           $this->userRepository->delete($id);
            return redirect()->back()->with('message', 'Deleted successfully');
        }
        return redirect()->back()->with('error', 'Account does not exist');
    }

    public function userDeleteAll(Request $request)
    {

        $arrUser = $request->get('checked_id');
        if($arrUser)
        {
            foreach ($arrUser as $key => $item) {
                $this->userRepository->delete($item);
            }
            return redirect()->back()->with('message', 'Deleted successfully');
        }
        return redirect()->back()->with('error', 'Account does not exist');

    }

    public function upgradeManger($id)
    {
       $is_Admin = [
           'is_Admin' => 1
       ];
       $this->userRepository->update($is_Admin,$id);
        return redirect()->back()->with('message', 'Update successfully');
    }

    public function updateMember($id)
    {
        $is_Admin = [
            'is_Admin' => null
        ];
        $this->userRepository->update($is_Admin,$id);
        return redirect()->back()->with('message', 'Update successfully');
    }
}
