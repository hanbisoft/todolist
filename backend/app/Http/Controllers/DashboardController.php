<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\AttachmentEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use App\Repositories\Eloquent\TaskEloquentRepository;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\TaskM;
use Input;
use Yajra\DataTables\DataTables;
use Session;
use App;
use Config;
class DashboardController extends Controller
{
    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $attachmentRepository;
    private $userRepository;
    private $taskRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        AttachmentEloquentRepository $attachmentRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository,
        TaskEloquentRepository $taskRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->attachmentRepository = $attachmentRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $id = Auth::user()->id;
        $tasks = $this->taskRepository->showMyTask($id);
        $countTask = $this->taskRepository->countTask($id);

        $project = $this->projectRepository->count();
        $user = $this->userRepository->count();
        if(Auth::user()->is_Admin === 1)
        {
            return view('admin.dashboard-manager');
        }
        return view('admin.dashboard',compact(['tasks','user','project','countTask','locale']));
//        return view('admin.calendar.index');
    }


    public function showMyTaskToday()
    {
        $id = Auth::user()->id;
        $task = $this->taskRepository->showMyTaskToday($id);
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $token = $encrypter->encrypt(csrf_token());
        // return $token;
        return Datatables::of($task)
            ->addColumn('action', function ($task) use ($token) {
                return '
                 <a class="btn btn-sm btn-info btn-round" href="/project-task/' . $task->id . '"> <i class="fas fa-eye"></i></a>
                ';
            })
            ->make(true);
    }
}
