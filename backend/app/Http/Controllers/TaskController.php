<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\AttachmentEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use App\Repositories\Eloquent\TaskEloquentRepository;
use Auth;
use App\Models\Attachment;
use App\Models\TaskLog;
use Yajra\Datatables\Datatables;
use Session;
use App;
use Config;
use Mail;
use App\Jobs\SendMailForDues;
use Carbon\Carbon;

class TaskController extends Controller
{
    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $attachmentRepository;
    private $userRepository;
    private $taskRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        AttachmentEloquentRepository $attachmentRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository,
        TaskEloquentRepository $taskRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->attachmentRepository = $attachmentRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function showMyTaskData(Request $request)
    {
        $id = Auth::user()->id;
        $task = $this->taskRepository->showMyTask($id);
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $token = $encrypter->encrypt(csrf_token());
        // return $token;
        return Datatables::of($task)
            ->addColumn('action', function ($task) use ($token) {
                return '
                 <a class="btn btn-sm btn-info btn-round btn-register btn_center" href="/project-task/' . $task->id . '"> <i class="fas fa-eye"></i></a>
                ';
            })
            ->make(true);
    }

    // <a href="/project-task/' . $task->id . '/edit">  <i class="fas fa-edit"></i></a>

    public function apiShowTask(Request $request)
    {
        // return "OK";
        $p_code = $request->get('p_code');
        $assign_to = $request->get('assign_to');
        $task = $this->taskRepository->showTask($p_code, $assign_to);
        return response()->json([
            'code' => 200,
            'data' => $task
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $project = $this->projectRepository->all();
        $company_id = Auth::user()->c_code;
        $user = $this->userRepository->findByField('c_code', $company_id);
        return view('admin.task.create', compact(['project', 'user']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        return $request->all();
        $arrUser = $request->get('arrUser');

        $user = Auth::user();
        if ($request->get('arrUser')) {
            foreach ($arrUser as $item) {
                $task = $this->taskRepository->create([
                    'c_code' => $user->c_code,
                    'tSubject' => $request->get('tSubject'),
                    't_sDate' => $request->get('t_sDate'),
                    't_eDate' => $request->get('t_eDate'),
                    't_diff' => $request->get('t_diff'),
                    't_Pred' => $request->get('t_Pred'),
                    't_Contents' => $request->get('t_Contents'),
                    't_Type' => $request->get('t_Type'),
                    't_Process' => $request->get('t_Process'),
                    't_Done' => 0,
                    'parent_id' => null,
                    'assign_to' => $item,
                    'p_code' => $request->get('p_code'),
                    'created_task_name' => Auth::user()->u_nickname
                ]);

            }
            $assign_email = $this->userRepository->findWhereIn('id',$arrUser);
            $email_manager = Auth::user()->email;
            foreach ($assign_email as $value) {
                $data = [
                    'email_member' => $value->email,
                    'email_manager' => $email_manager,
                    'name' => $value->u_nickname,
                    'task_name' => $task->tSubject,
                    'task_id' => $task->id,
                    'assign_by' => Auth::user()->u_nickname,
                ];
//                 dispatch(new SendMailForDues($data))->delay(now()->addMinutes(1));
                $email = $data['email_member'];
                $emailManager = $data['email_member'];
                Mail::send(/**
                 * @param $message
                 */ 'admin.mail.newtask', array(
                    'email' => $data['email_member'],
                    'name' => $data['name'],
                    'task_name' => $data['task_name'],
                    'assign_by' => $data['assign_by'],
                    'task_id' => $data['task_id'],
                ), function ($message)use($email,$email_manager) {
                    $message->to($email, 'NEW TASK')->subject('You has new Task!');
                });
            }


        } else {
            $task = $this->taskRepository->create([
                'c_code' => $user->c_code,
                'tSubject' => $request->get('tSubject'),
                't_sDate' => $request->get('t_sDate'),
                't_eDate' => $request->get('t_eDate'),
                't_diff' => $request->get('t_diff'),
                't_Pred' => $request->get('t_Pred'),
                't_Contents' => $request->get('t_Contents'),
                't_Type' => $request->get('t_Type'),
                't_Process' => $request->get('t_Process'),
                't_Done' => 0,
                'parent_id' => null,
                'assign_to' => null,
                'p_code' => $request->get('p_code'),
                'created_task_name' => Auth::user()->u_nickname
            ]);
        }
        //upload attachment file
        if ($request->hasFile('a_code')) {
            $excel = $request->file('a_code')->store('public/documents');
            $url_excel = $excel;
            $attachment = Attachment::create([
                'c_code' => $user->c_code,
                't_code' => $task->id,
                'a_file' => $url_excel,
                'a_fileName' => $request->file('a_code')->getClientOriginalName(),
                'a_date' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
        }
        return redirect()->back()->with('message', 'Create successful!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = $this->taskRepository->showTaskID($id);
        $sub_task = $this->taskRepository->subTask($id);
        $attachment = $this->attachmentRepository->findByField('t_code', $id);
//        return $task;
        $log = TaskLog::where('t_code', $id)->orderBy('created_at', 'desc')->limit(10)->get();
        return view('admin.task.show', compact(['task', 'log', 'id', 'sub_task', 'attachment']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks = $this->taskRepository->showTaskID($id);
        $projectID = '';
        foreach ($tasks as $t)
        {
            $projectID = $t->p_code;
        }
        $project = $this->projectRepository->all();
        $user = $this->taskRepository->showUserInTaskUpdate($projectID);
//        return $user;
        return view('admin.task.edit', compact(['id', 'project', 'user', 'tasks']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $user = Auth::user();
        // luu vao commit
        $task_id = $this->taskRepository->find($id);
         $user_task = $this->userRepository->find($task_id->assign_to);
        //log
        $task_commit = TaskLog::create([
            't_code' => $id,
            'l_status' => "Updated by: " . '<span class="text-info">' . $user->u_nickname . '</span>' . " about: " . '<span class="text-info">' . date('Y-m-d H:i:s') . '</span>' . " ago ",
            'l_content' => $request->get('task_commit'),
            'l_user_id' => $request->get('assign_to'),
            'time_log' => date('Y-m-d H:i:s')
        ]);
        //update task
        $task = $this->taskRepository->update($request->all(), $id);
        $email = $user_task->email;
        //send mail
        Mail::send(/**
         * @param $message
         */ 'admin.mail.updateTask', array(
             'id' => $task_id->id,
            'email' => $user_task->email,
            'name' => $user_task->u_nickname,
            'task_name' => $task_id->tSubject,
            'change_by' => $user->name ,
            'content' => "Updated by: " . '<span class="text-info">' . $user->u_nickname . '</span>' . " about: " . '<span class="text-info">' . date('Y-m-d H:i:s') . '</span>' . " ago ",
        ), function ($message)use($email) {
            $message->to($email, 'TASK UPDATE')->subject('The task assigned to you has new updates!');
        });

        //upload attachment file
        if ($request->hasFile('a_code')) {
            $excel = $request->file('a_code')->store('public/documents');
                $url_excel = $excel;
            $attachment = Attachment::create([
                'c_code' => $user->c_code,
                't_code' => $task->id,
                'a_file' => $url_excel,
                'a_fileName' => $request->file('a_code')->getClientOriginalName(),
                'a_date' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
        }
        return redirect('/project-task/' . $id)->with('message', 'Update successful!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $getId = $id;
        $task = $this->taskRepository->delTask($id);
        return redirect()->back()->with('message', 'Delete successful!');
    }

    public function addMember($id)
    {
        $company_id = Auth::user()->c_code;
        $task = $this->taskRepository->find($id);
        $user = $this->taskRepository->apiShowUserByIDProject($task->p_code);
//        return $user;
//        return $user;
        return view('admin.task.create_sub_task', compact(['id', 'user', 'company_id', 'task']));
    }
    /// create member adn sub task

    public function createMemberSubTask(Request $request)
    {
        $arrUser = $request->get('arrUser');

        $user = Auth::user();
        if ($request->get('arrUser')) {
            foreach ($arrUser as $item) {
                $task = $this->taskRepository->create([
                    'c_code' => $user->c_code,
                    'tSubject' => $request->get('tSubject'),
                    't_sDate' => $request->get('t_sDate'),
                    't_eDate' => $request->get('t_eDate'),
                    't_diff' => $request->get('t_diff'),
                    't_Pred' => $request->get('t_Pred'),
                    't_Contents' => $request->get('t_Contents'),
                    't_Type' => $request->get('t_Type'),
                    't_Process' => $request->get('t_Process'),
                    't_Done' => 0,
                    'parent_id' => $request->get('parent_id'),
                    'assign_to' => $item,
                    'p_code' => $request->get('p_code'),
                ]);
            }
        } else {
            $task = $this->taskRepository->create([
                'c_code' => $user->c_code,
                'tSubject' => $request->get('tSubject'),
                't_sDate' => $request->get('t_sDate'),
                't_eDate' => $request->get('t_eDate'),
                't_diff' => $request->get('t_diff'),
                't_Pred' => $request->get('t_Pred'),
                't_Contents' => $request->get('t_Contents'),
                't_Type' => $request->get('t_Type'),
                't_Process' => $request->get('t_Process'),
                't_Done' => 0,
                'parent_id' => $request->get('parent_id'),
                'assign_to' => null,
                'p_code' => $request->get('p_code'),
            ]);
        }
        //upload attachment file
        if ($request->hasFile('a_code')) {
            $excel = $request->file('a_code')->store('public/documents');
            $url_excel = $excel;
            $attachment = Attachment::create([
                'c_code' => $user->c_code,
                't_code' => $task->id,
                'a_file' => $url_excel,
                'a_fileName' => $request->file('a_code')->getClientOriginalName(),
                'a_date' => date("Y-m-d"),
                'user_id' => $user->id
            ]);
        }

        return redirect('/calendar-admin-task')->with('message', 'Create Subtask successful!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myTask()
    {

        return view('admin.task.mytask');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendarMyTask()
    {
        $user = Auth::user();
        $project = $this->projectItemRepository->findByField('u_id', $user->id);
        return view('admin.calendar.my-task-calendar', compact('project'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiShowMyTask()
    {
        $user = Auth::user();
        $id = $user->id;
        $task = $this->taskRepository->showMyTask($id);
        $arrData = [];


        foreach ($task as $value) {
            $color =  "";
            if($value->task_type == "Development")
            {
                $color = Config::get('color.DEV');
            }
            elseif ($value->task_type == "Unit Testing"){
                $color = Config::get('color.TEST');
            }
            elseif ($value->task_type == "Update"){
                $color = Config::get('color.UPDATE');
            }
            elseif ($value->task_type == "Other"){
                $color = Config::get('color.ETC');
            }
            elseif ($value->task_type == "Bug"){
                $color = Config::get('color.DEBUG');
            }
            $end_date = $value->t_eDate;
            $end_date1 = str_replace('-', '/', $end_date);
            $tomorrow = date('Y-m-d', strtotime($end_date1 . "+1 days"));

            $data = [
                'id' => $value->id,
                'start' => $value->t_sDate,
                'end' => $tomorrow,
                'title' => $value->tSubject . ' - '.  $value->t_Done. '%' . ' - ' . $value->u_nickname,
                'backgroundColor' => $color,
                'eventColor' => '#ffffff',
            ];
            array_push($arrData, $data);
        }
        return response()->json($arrData);
    }
    /**
     * @param Request $request
     */
    public function apiSearchMyTask(Request $request)
    {
        $p_code = $request->get('p_code');
        $assign_to = $request->get('assign_to');
        $user = Auth::user();
        $id = $user->id;
        $task = $this->taskRepository->searchMyTask($assign_to,$p_code);
        $arrData = [];
        foreach ($task as $value) {
            $color =  "";
            if($value->task_type == "Development")
            {
                $color = Config::get('color.DEV');
            }
            elseif ($value->task_type == "Unit Testing"){
                $color = Config::get('color.TEST');
            }
            elseif ($value->task_type == "Update"){
                $color = Config::get('color.UPDATE');
            }
            elseif ($value->task_type == "Other"){
                $color = Config::get('color.ETC');
            }
            elseif ($value->task_type == "Bug"){
                $color = Config::get('color.DEBUG');
            }
            $end_date = $value->t_eDate;
            $end_date1 = str_replace('-', '/', $end_date);
            $tomorrow = date('Y-m-d', strtotime($end_date1 . "+1 days"));
            $data = [
                'id' => $value->id,
                'start' => $value->t_sDate,
                'end' => $tomorrow,
                'title' => $value->tSubject . ' - '.  $value->t_Done. '%' . ' - ' . $value->u_nickname,
                'backgroundColor' =>  $color,
                'eventColor' => '#ffffff',
            ];
            array_push($arrData, $data);
        }
        return response()->json($arrData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchMyTaskCalendar(Request $request)
    {
        $p_code = $request->get('p_code');
        $user = Auth::user();
        $project = $this->projectItemRepository->findByField('u_id', $user->id);
        return view('admin.calendar.search-my-task', compact(['project']));
    }


    public function apiShowDetailTask(Request $request)
    {
        $id = $request->get('id');
        if($id) {
            $task = $this->taskRepository->showTaskID($id);
            $task_check = $this->taskRepository->find($id);
//            return $task_check;
            $user = $this->taskRepository->apiShowUserByIDProject($task_check->p_code);
            $sub_task = $this->taskRepository->subTask($id);
            $attachment = $this->attachmentRepository->findByField('t_code', $id);
            $log = TaskLog::where('t_code', $id)->orderBy('created_at', 'desc')->limit(10)->get();
            $data = [
                'task' => $task,
                'sub_tasl' => $sub_task,
                'attachment' => $attachment,
                'log' => $log,
                'user' => $user
            ];
            return response()->json($data);
        }else
        {
            $data = [
                'task' => null,
                'sub_tasl' => null,
                'attachment' => null,
                'log' => null
            ];
            return response()->json($data);
        }
    }
}
