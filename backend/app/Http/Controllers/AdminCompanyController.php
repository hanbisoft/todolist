<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use Yajra\DataTables\DataTables;

class AdminCompanyController extends Controller
{
    private $companyRepository;

    function __construct(
        CompanyEloquentRepository $companyRepository
    )
    {
        $this->companyRepository = $companyRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.company.index');
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function showData()
    {
        $company = $this->companyRepository->all();
        $encrypter = app('Illuminate\Encryption\Encrypter');
        $token = $encrypter->encrypt(csrf_token());
        return Datatables::of($company)
            ->addColumn('action', function ($company) use ($token) {
                return '
            <a class="btn  btn-sm btn-success btn-register"  style="margin-right:10px" href="/company/' . $company->id . '/edit"> <i class="far fa-edit"></i></a>
            <a class="btn btn-sm btn-danger btn-register" href="javascript:void(0);" onclick="return confirmDelete(' . $company->id . ')"> <i class="fas fa-trash-alt"></i></a>
             <form action="company-delete" method="get" id="frm_delete_' . $company->id . '">
                <input type="hidden" name="id" value='.$company->id.'>
            </form>';
            })
            ->make(true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = $this->companyRepository->create($request->all());
        return redirect()->back()->with('message','Create successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $company = $this->companyRepository->find($id);
       return view('admin.company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = $this->companyRepository->update($request->all(),$id);
        return redirect()->back()->with('message','Update successfully');
    }

    public function destroy($id)
    {

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function destroyCompany(Request $request)
    {
        $id = $request->get('id');
        $company = $this->companyRepository->delete($id);
        return redirect()->back()->with('message','Deleted successfully');
    }


}
