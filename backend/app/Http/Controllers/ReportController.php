<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Eloquent\ProjectEloquentRepository;
use App\Repositories\Eloquent\ProjectItemEloquentRepository;
use App\Repositories\Eloquent\CompanyEloquentRepository;
use App\Repositories\Eloquent\AttachmentEloquentRepository;
use App\Repositories\Eloquent\UserEloquentRepository;
use App\Repositories\Eloquent\TaskEloquentRepository;
use Auth;
class ReportController extends Controller
{
    private $projectRepository;
    private $projectItemRepository;
    private $companyRepository;
    private $attachmentRepository;
    private $userRepository;
    private $taskRepository;

    function __construct(
        ProjectEloquentRepository $projectRepository,
        CompanyEloquentRepository $companyRepository,
        AttachmentEloquentRepository $attachmentRepository,
        UserEloquentRepository $userRepository,
        ProjectItemEloquentRepository $projectItemRepository,
        TaskEloquentRepository $taskRepository
    )
    {
        $this->projectRepository = $projectRepository;
        $this->companyRepository = $companyRepository;
        $this->attachmentRepository = $attachmentRepository;
        $this->userRepository = $userRepository;
        $this->projectItemRepository = $projectItemRepository;
        $this->taskRepository = $taskRepository;
    }


    public function index()
    {
        $project = $this->projectRepository->all();
        
        return view('admin.report.index',compact('project'));
    }

    public function apiShowCountTask(Request $request)
    {
       $start_date = $request->get('start_date');
       $end_date = $request->get('end_date');
       $user = Auth::user()->id;     
       $id = $request->get('id');
       $project = $this->projectRepository->find($id);
       if($project)
       {
           $totals = $this->projectRepository->countTaskProject($id);
           $done  = $this->projectRepository->countTaskProjectDone($id);
           $delay  = $this->projectRepository->CountTaskProjectDelay($id);
           $task = $this->projectRepository->showTask($id,$user);
           return response()->json([
               'total' => $totals,
               'done' => $done,
               'delay' => $delay,
               'pName' =>$project->pName,
               'task' => $task,
           ]);
       }
       return response()->json([
           'total' => null,
           'done' => null,
           'delay' => null
       ]);
      

    }
}
