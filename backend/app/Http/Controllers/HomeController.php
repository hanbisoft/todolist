<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App;
use Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $check = Auth::user()->u_auth;
        if ($check == 1) {
            return redirect('/admin-dashboard');
        } else {
            return view('home');
        }

    }

    /**
     * @param $language
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeLanguage($locale)
    {
//        return $locale;
        Session::put('locale', $locale);
        App::setLocale(Session::get('locale'));
        return redirect()->back();
    }

}
