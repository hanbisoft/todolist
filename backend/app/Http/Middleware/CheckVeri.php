<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckVeri
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user() &&  Auth::user()->u_auth == 1) {
            return $next($request);
        }

        return redirect('/login');
    }
}
