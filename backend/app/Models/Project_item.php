<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project_item extends Model
{
    protected $fillable = [
        'c_code',
        'p_name_item',
        'u_id',
        'pu_Auth',
        'p_code'
        ];
}
