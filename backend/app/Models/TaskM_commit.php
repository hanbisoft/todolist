<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskM_commit extends Model
{
    protected $table = 'task_m_commits';
    protected $fillable = [
        'c_code',
        'p_code',
        't_code',
        'tcom_Contents',
        'u_id',
        'tcDate'
        ];
}
