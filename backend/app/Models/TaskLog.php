<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskLog extends Model
{
    protected $table = 'task_log';
    protected $fillable = [
        't_code',
        'l_status',
        'l_content',
        'l_user_id',
        'time_log'
        ];
}
