<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'c_name',
        'c_tel',
        'c_email',
        'c_id'
    ];
}
