<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
        'c_code',
        'p_code',
        'a_file',
        'a_date',
        'user_id',
        'a_fileName',
        't_code'
        ];
}
