<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
    'c_code',
    'pName',
    'psDate',
    'peDate',
    'pType',
    'pContents',
    'pAmount',
    ];

}
