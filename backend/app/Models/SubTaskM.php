<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubTaskM extends Model
{
    protected $table = 'sub_task_m_s';
    protected $fillable = [
        'c_code',
        'p_code',
        't_code',
        'tsub_sDate',
        'tsub_eDate',
        'tsub_Contents',
        'tsub_name'
        ];
}
