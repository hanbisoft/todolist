<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubTaskM_Item extends Model
{
    protected $table = 'sub_task_m__items';
    protected $fillable = [
        'c_code',
        'p_code',
        't_code',
        'user_id'
        ];
}
