<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskM extends Model
{

    protected $table = 'task_m_s';
    protected $fillable = [
        'c_code',
        'p_code',
        'tSubject',
        't_sDate',
        't_eDate',
        't_diff',
        't_Pred',
        't_Contents',
        't_Type',
        't_Process',
        't_Done',
        'parent_id',
        'assign_to',
        't_Done',
        'created_task_name'
        ];

}
