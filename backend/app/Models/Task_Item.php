<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task_Item extends Model
{
    protected $table = 'task__items';
    protected $fillable = [
        'c_code',
        'p_code',
        't_code',
        'user_id'
        ];
}
